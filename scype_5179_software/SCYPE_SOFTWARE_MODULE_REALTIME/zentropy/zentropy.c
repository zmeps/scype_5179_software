/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include <openssl/sha.h>

#include "../../str.h"
#include "../../sr_module.h"
#include "../../dprint.h"
#include "../../error.h"
#include "../../ut.h"
#include "../../mem/mem.h"
#include "../../mem/shm_mem.h"
#include "../../ut.h"
#include "../../usr_avp.h"
#include "../../data_lump_rpl.h"
#include "../../parser/parse_expires.h"
#include "../../parser/parse_event.h"
#include "../../parser/contact/parse_contact.h"
#include "../../lib/kcore/hash_func.h"
#include "../../lib/kcore/parser_helpers.h"

#include "../../str_hash.h"

#include "../pua/hash.h"
#include <sys/time.h>
#include "../../cfg/cfg.h"

MODULE_VERSION

// Function prototype decleration
static int mod_init(void);
static int w_zentropy(struct sip_msg*);
int check_existence(char *);
int compute_metrics(char *[]);
int computeHash(char *, char *);

// Global Variables
GHashTable *Sip_income;
int registers=0, acks=0, byes=0, invites=0, options=0, cancels=0, TotalMessages=0, attackMessages=0, normalMessages=0, realAttackMessages=0, realNormalMessages=0, fakeAttackMessage=0;
// This variable corresponds to the message window constant
const int TimeWindowMessages=1000;

float MeanValue=0.0, AvgDistance=11.14, tempVariance=0.0, ATE=0.0;
int flag = 0, flagRealInvite = 0;
FILE *timeFile=NULL, *rounds=NULL;

// Defines the exported function
static cmd_export_t cmds[]={

        {"zentropy",  w_zentropy, 0, 0, 0, REQUEST_ROUTE},
	{0,0,0,0,0,0}
};

// This function defines parameters of the exported functions
struct module_exports exports= {
	"zentropy",
	DEFAULT_DLFLAGS, /* dlopen flags */
	cmds,
	0,
	0,          /* exported statistics */
	0,          /* exported MI functions */
	0,          /* exported pseudo-variables */
	0,          /* extra processes */
	mod_init,
	0,
	0,
	0,           /* per-child init function */
};

// This function is called when the module starts 
static int mod_init(void)
{
        // Generate appropriate files for writing
        timeFile = fopen("/SCENARIO.txt","w+");
        fprintf(timeFile, "No, Inf1,    Inf2,     Inf3,    Inf4,     inf5,     Inf6,     Probability,      Entropy,       TheoMax,       StDeviation,        comp,        diff,       MaxNormAver,      ActInfoNormAver,        Distance,       ATE,       TIME\n");
        fclose(timeFile); 
        rounds = fopen("/rounds.txt", "w+");
        fprintf(rounds, "------\n");
        fclose(rounds);
		// Create hash table
        Sip_income = g_hash_table_new(g_str_hash, g_str_equal);
        DBG("ZENTROPY FLOOD DETECTION MODULE :-> ZENTROPY MODULE");
		
	return 0;
}

// This function is triggered every time the server takes a new SIP INVITE message
static int w_zentropy(struct sip_msg* msg)
{  
    int i=0, digestLen=65;
	
    // We should increase the total messages if we take one new
    TotalMessages++;
    DBG("\n Print the total number of messages : %d \n", TotalMessages); 

   ///////////////////////////////////////////////////////////////////////////
   // If messages exceed the message window then start calculating the FPs and FNs
   if((TotalMessages > TimeWindowMessages))
   {
        // Writing results in a file
        DBG("\n !!!! *** HASH TABLE SIZE ... %d", g_hash_table_size(Sip_income));
        rounds = fopen("/rounds.txt", "a+");
        fprintf(rounds,"\n*********************************************");
        fprintf(rounds,"Hash table size : [%d] Total number of messages : [%d]", g_hash_table_size(Sip_income), TotalMessages);
        fprintf(rounds,"\nTotal attack messages : [%d]", realAttackMessages); 
        fprintf(rounds,"\nTotal TP attack messages : [%d]", attackMessages); 
        fprintf(rounds,"\nTotal FP messages : [%d]", fakeAttackMessage);
        fprintf(rounds,"\nTotal normal messages : [%d]", realNormalMessages); 
        fprintf(rounds,"\nTotal FN messages : [%d]", normalMessages); 
        fprintf(rounds,"\n*********************************************"); 
        fprintf(rounds,"\nTotal number of INVITE requests : [%d]", invites);
        fclose(rounds); 

		// If the round is completed we should destroy the hash table
        g_hash_table_destroy(Sip_income);
        //Sip_income = NULL;
		// And generate a new one for the next round
        Sip_income = g_hash_table_new(g_str_hash, g_str_equal);

        TotalMessages = 1;
        MeanValue=0.0, tempVariance=0.0, ATE=0.0;
   }

    ///////////////////////////////////////////
    struct timeval tcp_start, finish;
    double d_tcp_start, d_finish;
   
    gettimeofday(&tcp_start,NULL);
                d_tcp_start=tcp_start.tv_sec+(tcp_start.tv_usec/1000000.0);

    parse_headers(msg, HDR_EOH_F, 0);
    flag=0; 

    // TUPWNW OLO TO MINIMA...
    DBG("\nSTARTED !!!\n");
    DBG("\nThis is the input MESSAGE... %s\n", msg->buf);
   
    // Declare appropriate tables to copy the contents of the message per header of interest
    char *temp[6]; 
    char tempor1[100];
    char tempor2[100];   
    char *oBuffer[6];

	// Allocate memory to copy the requested contents of the message
    temp[0] = (char *)malloc(sizeof(char)* ((msg->first_line.u.request.method.len + msg->first_line.u.request.uri.len)+100) );
    temp[1] = (char *)malloc(sizeof(char)* ((msg->h_via1->body.len)+10) );
    temp[2] = (char *)malloc(sizeof(char)* ((msg->from->body.len)+10) );
    temp[3] = (char *)malloc(sizeof(char)* ((msg->to->body.len)+10) );
    temp[4] = (char *)malloc(sizeof(char)* ((msg->callid->body.len)+10) );
   
   // Allocate memory to store the digest for every header
   for(i=0; i<6; i++)
   {
      oBuffer[i] = (char *)malloc(sizeof(char)*digestLen);  
   }
   
   // Copy operation
   snprintf(tempor1, msg->first_line.u.request.method.len +1, msg->first_line.u.request.method.s);
   DBG("\n Copying... %s\n", tempor1);
   snprintf(tempor2, msg->first_line.u.request.uri.len +1, msg->first_line.u.request.uri.s);
   strcat(tempor1, tempor2); 
   sprintf(temp[0], tempor1);
   DBG("\n Final contents will be equal to %s\n", temp[0]);

   // According to the incoming message increase counters
   if(strstr(tempor1, "REGISTER")) 
   { 
      registers++;
   }
   else if(strstr(tempor1, "ACK"))
   {   flag = 1;
       acks++;
   } 
   else if(strstr(tempor1, "BYE"))
   {
       flag=1;
       byes++;
   }
   else if(strstr(tempor1, "INVITE"))
   {
      invites++;
   }
   else if(strstr(tempor1, "OPTION"))
   {  
      options++;
   }
   else if(strstr(tempor1, "CANCEL"))
   {
      cancels++;
   }
 
   snprintf(temp[1], msg->h_via1->body.len, msg->h_via1->body.s); 
   snprintf(temp[2], msg->from->body.len, msg->from->body.s); 
   snprintf(temp[3], msg->to->body.len, msg->to->body.s); 
   snprintf(temp[4], msg->callid->body.len,msg->callid->body.s);
   
   if(flag==1)
   {
      temp[5] = (char *)malloc(sizeof(char)* ((msg->contact->body.len)+2) );
      snprintf(temp[5], msg->callid->body.len, "<--->");
   }
   else 
   {
      temp[5] = (char *)malloc(sizeof(char)* ((msg->contact->body.len)+2) );
      snprintf(temp[5], msg->contact->body.len,msg->contact->body.s);
   }

   if(strstr(temp[1] ,":9")!=NULL) 
   { 
     realAttackMessages++; 
     flagRealInvite = 1;
     DBG("\n!! This is a TP message !!"); 
   }
   else
   { 
     realNormalMessages++;
     DBG("\n!! This is a normal message !!"); 
   }

   // Insert the headers to the hash table  
   for(i=0; i<6; i++)
   {
       computeHash(temp[i], oBuffer[i]);
       check_existence(oBuffer[i]);
   }
                   
   // Compute metrics
   compute_metrics(oBuffer);
   DBG("\n*********************************************"); 
   DBG("\nTotal attack messages : [%d]", realAttackMessages); 
   DBG("\nTotal TP attack messages : [%d]", attackMessages); 
   DBG("\nTotal FP messages : [%d]", fakeAttackMessage);
   DBG("\nTotal normal messages : [%d]", realNormalMessages); 
   DBG("\nTotal FN messages : [%d]", normalMessages); 
   DBG("\n*********************************************"); 
   DBG("\nTotal INVITE requests : [%d]", invites);
   DBG("\nTotal ACK requests : [%d]", acks);  
   DBG("\nTotal BYE requests : [%d]", byes); 
   DBG("\nTotal CANCEL requests : [%d]", cancels);
   DBG("\nTotal OPTION requests: [%d]", options);
   DBG("\nTotal REGISTER requests : [%d]", registers);

   // Free allocated memory
   for(i=0; i<6; i++)
   {
      free(temp[i]);
      free(oBuffer[i]);
   }
 
   gettimeofday(&finish,NULL);
            d_finish=finish.tv_sec+(finish.tv_usec/1000000.0);

   DBG("\nThe time which was required to process the message will be equal to : [%lf]", d_finish-d_tcp_start);  
   timeFile = fopen("/SCENARIO.txt", "a+");
   fprintf(timeFile," %lf ", d_finish-d_tcp_start);
   fclose(timeFile);   
   return 1;   
}

// This function calculates the various matrics related to the entropy
int compute_metrics(char *temp[])
{
    // Values from normal scenario part 3 (contains 43672 messages).. we suppose that these values are precomputed and possibly imported in the config file.
    // Prob = 1/43672 = 0.0000228
    // -log2(Prob) = 15.42 
    // 1. MeanValue(normalscenario-part3) = 57.48 , StDeviation(normalscenario-part3) = 4.44
    //
    // 2. TheoMax(normalscenario-part3) = 6* -1 * log2( (float)1 /number_of_messages) 
    // = 15.42*6 = 92.52
    //
    // 3. ComputationThreshold = TheoMax(normalscenario-part3) - MeanValue(normalscenario-part3)    + StDeviation(normalscenario-part3) = 92.52-57.48+4.44 = 39.48

    //float inf1, inf2, inf3, inf4, inf5, inf6, inf7, MeanValue

    int i=0;
    float inf[6]={0.0}, ActualInfo=0.0, TheoMax=0.0, Entropy=0.0, Probability=0.0, StDeviation=0.0, NormAverThreshold =25.79, comp=0.0, diff=0.0, MaxNormAver=0.0, ActInfoNormAver=0.0, Distance=0.0, Variance=0.0, tmp=0.0;
   
    timeFile = fopen("/SCENARIO.txt", "a+");
    fprintf(timeFile,"\n %d, ", TotalMessages);
    fclose(timeFile);

    for(i=0; i<6; i++)
    {
	    if((flag==1) && (i==5))
	       continue;
	   
	    // Calculate the information meter for the specific symbol
	    inf[i] = -1*log2((float) GPOINTER_TO_INT(g_hash_table_lookup(Sip_income, temp[i]))/TotalMessages);

	    timeFile = fopen("/SCENARIO.txt", "a+");
	    fprintf(timeFile," %lf, ", inf[i]);
	    fclose(timeFile); 

	    DBG("\n Symbol %d has %d number of occurences\n",i+1 ,GPOINTER_TO_INT(g_hash_table_lookup(Sip_income, temp[i])));  
	    DBG("\n The calculated information meter will be equal to : %f \n",i+1 , inf[i]); 

	    ActualInfo += inf[i];
    }
   
    // Compute Metrics
    //ActualInfo = inf1 + inf2 + inf3 + inf4 + inf5 + inf6;
    MeanValue += ActualInfo;
    tmp = MeanValue/TotalMessages;

    Probability = (float)1/TotalMessages;

    Entropy = -1*log2(Probability);
    ATE +=ActualInfo;     
    TheoMax = 6*Entropy;   
    tempVariance += pow( (ActualInfo - tmp) , 2);
    Variance = tempVariance * ((float)1/TotalMessages);    
    StDeviation = sqrt(Variance);
       
    // This line should be uncommented in the train phase
    //NormAverThreshold = TheoMax - tmp + StDeviation;
    
    comp = (ActualInfo - NormAverThreshold) + StDeviation;
    diff = (NormAverThreshold - comp)/NormAverThreshold;
    MaxNormAver = TheoMax - NormAverThreshold;
    ActInfoNormAver = ActualInfo - NormAverThreshold;
    Distance = MaxNormAver - ActInfoNormAver;

    timeFile = fopen("/SCENARIO.txt", "a+");
    fprintf(timeFile," %lf, %lf, %lf, %lf , %lf , %lf , %lf , %lf , %lf, %lf,",Probability, Entropy, TheoMax, StDeviation, comp, diff, MaxNormAver, ActInfoNormAver, Distance, ATE);
    fclose(timeFile); 

    ////////////////////////////////////////////////////////////////////////
	// This part should be uncommented in the train phase
    /*
	// These lines of code write in a file the avg distance (AID parameter) for a specified message window
    if(TotalMessages <= TimeWindowMessages)
    {
       AvgDistance += Distance;
       fopen("/home/meps-voip/Desktop/WindowMessages.txt", "w+");
       fprintf(timeFile,"The Threshold : Normal Average is : %lf,\n The Avg(Distance) is : %lf", NormAverThreshold, AvgDistance/TimeWindowMessages);
       fclose(timeFile);
    }
    */
    ///////////////////////////////////////////////////////////////////

	// Detection Phase
    // if the distance of the current message is greater than the NAD then it is classified as attack	
    if(Distance > (AvgDistance+StDeviation))
    {
       if(flagRealInvite==1) 
       {
          attackMessages++;
       }
       else
       {
          fakeAttackMessage++;
       }
       DBG("\n @@@@@ This is an attack message ");
       flagRealInvite=0;
    }
    else
    {
       normalMessages++;
       DBG("\n !!!!! This is a normal message ");
    }

    DBG("\n -----------------------------------------");
    DBG("\n Messages classified as attack : [%d]... ", attackMessages);
    DBG("\n -----------------------------------------");
    DBG("\n Messages classified as normal : [%d]... ", normalMessages);
    DBG("\n -----------------------------------------");

    return 1;
}

// Insert a new record to the hash table if it does not already exists. Otherwise, increment by the value. 
int check_existence(char *temp)
{
    if(GPOINTER_TO_INT(g_hash_table_lookup(Sip_income, temp))==0)
    {           
        g_hash_table_insert(Sip_income, temp, GINT_TO_POINTER(1));
        return 1;
    }
    else
    {
        g_hash_table_insert(Sip_income, temp, g_hash_table_lookup(Sip_income, temp) + 1 ); 
        return 1;
    }
 
}

// This function computes the digest for a string based on the HMAC_SHA256 function
int computeHash(char *line , char *digest)
{
    // Define parameters
    unsigned char obuf[32];
    char *HMAC_Result=NULL, key[]="SCYPE5179_SECRET_KEY", *digest=NULL;
    int i, digestLen=65;
	
    // Compute HMAC_SHA256
    HMAC(EVP_sha256(), key, strlen(key), (unsigned char *)line, strlen(line), obuf, NULL);
    // Print the 64 digest in an appropriate buffer
    for (i = 0; i < 32; i++) 
    {
        snprintf(digest+i*2, 4, "%02x", obuf[i]);
    }
    return 1;
}
