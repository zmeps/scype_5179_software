#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include <glib.h>
#include <openssl/sha.h>

#include "../../str.h"
#include "../../sr_module.h"
#include "../../dprint.h"
#include "../../error.h"
#include "../../ut.h"
#include "../../mem/mem.h"
#include "../../mem/shm_mem.h"
//Parsed headers...
#include "../../ut.h"
#include "../../usr_avp.h"
#include "../../data_lump_rpl.h"
#include "../../parser/parse_expires.h"
#include "../../parser/parse_event.h"
#include "../../parser/contact/parse_contact.h"
#include "../../lib/kcore/hash_func.h"
#include "../../lib/kcore/parser_helpers.h"

#include "../../str_hash.h"

#include "../pua/hash.h"
#include <sys/time.h>
#include "../../cfg/cfg.h"
#include <jni.h>

// Define the value of Mw
#define Mw 3

MODULE_VERSION

//////////////////////////////////////////////////////
static int mod_init(void);
static int w_zipics(struct sip_msg*);
int checkExistence(GHashTable *, char *);
int computeDigest(char *, char *);

//////////////////////////////////////////////////////
JavaVM *jvm;
JNIEnv *env;
jclass WekaTest_class;
jclass hello_world_class;
jmethodID main_method;
jmethodID square_method;
jmethodID power_method;
jmethodID train_method;
jmethodID test_method;
jmethodID constructor;
jdoubleArray valArray;
jobject a;

FILE *timeFile;

GHashTable *Sip_income1=NULL;
GHashTable *Sip_income2=NULL;
int i=0, numOfInvites=0, flag=0, digestLen=65, occurences, rounds=0, TP=0, FP=0, FN=0, attackMessages=0;
double classification=0.0;
// Declare appropriate tables to copy the contents of the message per header of interest
    char *headersOfInterest[6]; 
    char tempor1[100];
    char tempor2[100];   
    char *headersDigest[6];
    double Array[6];
///////////////////////////////////////////////////////////////////

static cmd_export_t cmds[]={

        {"zipics",  (cmd_function)w_zipics, 0, 0, 0, REQUEST_ROUTE},
	{0,0,0,0,0, 0}
};

struct module_exports exports= {
	"zipics",
	DEFAULT_DLFLAGS, /* dlopen flags */
	cmds,
	0,
	0,          /* exported statistics */
	0,          /* exported MI functions */
	0,          /* exported pseudo-variables */
	0,          /* extra processes */
	mod_init,
	0,
	0,
	0,           /* per-child init function */
};

JNIEnv* create_vm(JavaVM **jvm)
{

    JNIEnv* env;
    JavaVMInitArgs args;
    JavaVMOption options;
    args.version = JNI_VERSION_1_6;
    args.nOptions = 1;
    options.optionString = "-Djava.class.path=/usr/local/src/kamailio-3.2/kamailio/modules_k/zIPICS2015/:/usr/local/src/kamailio-3.2/kamailio/modules_k/zIPICS2015/weka.jar";
    args.options = &options;
    args.ignoreUnrecognized = 0;
    int rv;
    rv = JNI_CreateJavaVM(jvm, (void**)&env, &args);
    if (rv < 0 || !env)
        printf("Unable to Launch JVM %d\n",rv);
    else
        printf("Launched JVM! :)\n");
    return env;
}


static int mod_init(void)
{

     Sip_income1 = g_hash_table_new(g_str_hash, g_str_equal);
     Sip_income2 = g_hash_table_new(g_str_hash, g_str_equal);  

     env = create_vm(&jvm);
     WekaTest_class = (*env)->FindClass(env, "WekaTest");
     train_method = (*env)->GetStaticMethodID(env, WekaTest_class, "train", "()V");
     (*env)->CallStaticVoidMethod(env, WekaTest_class, train_method, NULL);

     timeFile=fopen("/home/meps-voip/Desktop/WekaModels/times.txt", "w");
     fclose(timeFile);

     DBG("\nIPICS MODULE...\n_____******_____");
 
     return 0;
}

static int w_zipics(struct sip_msg* msg)
{  
     
     DBG("\nThe whole message is.. : %s", msg->buf);
     struct timeval tcp_start, finish;
     double d_tcp_start, d_finish;
   
     gettimeofday(&tcp_start,NULL);
     d_tcp_start=tcp_start.tv_sec+(tcp_start.tv_usec/1000000.0);

     if((++rounds)==1)
     {
        WekaTest_class = (*env)->FindClass(env, "WekaTest");
        valArray = (*env)->NewDoubleArray(env, 6); 
        test_method = (*env)->GetStaticMethodID(env, WekaTest_class, "test", "([D)D");
     }  

     parse_headers(msg, HDR_EOH_F, 0);

     // Allocate memory to copy the desired headers of the message
     headersOfInterest[0] = (char *)malloc(sizeof(char)* ((msg->first_line.u.request.method.len + msg->first_line.u.request.uri.len)+1));
     headersOfInterest[1] = (char *)malloc(sizeof(char)* ((msg->h_via1->body.len)+1));
     headersOfInterest[2] = (char *)malloc(sizeof(char)* ((msg->from->body.len)+1));
     headersOfInterest[3] = (char *)malloc(sizeof(char)* ((msg->to->body.len)+1));
     headersOfInterest[4] = (char *)malloc(sizeof(char)* ((msg->callid->body.len)+1));
     headersOfInterest[5] = (char *)malloc(sizeof(char)* ((msg->contact->body.len)+1));

     // Allocate memory to store the digest for every header
     for(i=0; i<6; i++)
       headersDigest[i] = (char *)malloc(sizeof(char)*digestLen); 

    // Copy operation
    snprintf(tempor2, msg->first_line.u.request.method.len+1, msg->first_line.u.request.method.s);
    DBG("\n Copying... %s\n", tempor2);
    snprintf(tempor1, msg->first_line.u.request.uri.len+1, msg->first_line.u.request.uri.s);
    strcat(tempor2, tempor1); 
    sprintf(headersOfInterest[0], tempor2);
            
    snprintf(headersOfInterest[1], msg->h_via1->body.len+1, msg->h_via1->body.s);
    snprintf(headersOfInterest[2], msg->from->body.len+1, msg->from->body.s);
    snprintf(headersOfInterest[3], msg->to->body.len+1, msg->to->body.s);
    snprintf(headersOfInterest[4], msg->callid->body.len+1, msg->callid->body.s);
    snprintf(headersOfInterest[5], msg->contact->body.len+1, msg->contact->body.s);

    if(numOfInvites%Mw==0)
    {
            if(flag==0)
            {
                 flag=1;
                 g_hash_table_remove_all(Sip_income1);
            }
            else
            {
                 flag=0;
                 g_hash_table_remove_all(Sip_income2);
            }  
    }		 
            
    for(i=0; i<6; i++)
    {
	    computeDigest(headersOfInterest[i], headersDigest[i]);
   
            if(flag==0)
            {
                      checkExistence(Sip_income2, headersDigest[i]);
                      DBG("******** !!!![%d]\n", GPOINTER_TO_INT(g_hash_table_lookup(Sip_income2, headersDigest[i])));
                      occurences = GPOINTER_TO_INT(g_hash_table_lookup(Sip_income1, headersDigest[i]));
                      DBG("******** !!!! [%d]\n", flag, occurences);
            }
            else
            {
                      checkExistence(Sip_income1, headersDigest[i]);
                      DBG("******** !!!! [%d]\n", GPOINTER_TO_INT(g_hash_table_lookup(Sip_income1, headersDigest[i])));
                      occurences = GPOINTER_TO_INT(g_hash_table_lookup(Sip_income2, headersDigest[i]));
                      DBG("******** !!!! [%d]\n", flag, occurences);
            }
                  
            Array[i] = occurences;
     }

     Array[0] = 1000;
     Array[1] = 1000;
     Array[2] = 1000;
     Array[3] = 1000;
     Array[4] = 1000;
     Array[5] = 1000;

     (*env)->SetDoubleArrayRegion(env, valArray, 0, 6, Array);
     classification = (*env)->CallStaticDoubleMethod(env, WekaTest_class, test_method, valArray); 

     numOfInvites++;

     
     if(strstr(headersOfInterest[5], ":9"))
     {
         attackMessages++;

         if(classification)
         {
               FN++;  
               DBG("\n!!!!!!--FN----- :%d", FN);
         }
         else
         {
               TP++;
               DBG("\n!!!!!!--TP----- :%d", TP);
         }
     }
     else      
     {
         if(classification)
         {
               FP++;
               DBG("\n!!!!!!--FP----- :%d", FP);
         }
     }    


     for(i=0; i<6; i++)
     {
         free(headersOfInterest[i]);
         free(headersDigest[i]);
     } 	    
 
     gettimeofday(&finish,NULL);
            d_finish=finish.tv_sec+(finish.tv_usec/1000000.0);

     DBG("\n*******!!!!**********This message was classified as...:%lf", classification);      

     timeFile = fopen("/home/meps-voip/Desktop/WekaModels/times.txt", "a+");
     fprintf(timeFile," %lf ", d_finish-d_tcp_start);
     fclose(timeFile);  

     /***********************************************************************
     WekaTest_class = (*env)->FindClass(env, "WekaTest");
     constructor = (*env)->GetMethodID(env, WekaTest_class,"<init>","()V");
     // = (*env)->NewObject(env, WekaTest_class, constructor);
     
     a = (*env)->NewObject(env, WekaTest_class, constructor, NULL);               
     test_method = (*env)->GetMethodID(env, WekaTest_class, "test", "([D)V");
     (*env)->CallVoidMethod(env, a, test_method, valArray);
     //(*env)->DeleteLocalRef(env,a);
     //(*env)->DeleteLocalRef(env,WekaTest_class);
     ***********************************************************************/  

   return 1;   
}
  

// Insert a new record to the hash table if it does not already exists. Otherwise, increment by the value. 
int checkExistence(GHashTable *Sip_income, char *headerDigest)
{
    char *digest=NULL;
    digest = strdup(headerDigest);
    if(GPOINTER_TO_INT(g_hash_table_lookup(Sip_income, digest))==0)
    {           
        g_hash_table_insert(Sip_income, digest, GINT_TO_POINTER(1));
        return 1;
    }
    else
    {
        g_hash_table_insert(Sip_income, digest, g_hash_table_lookup(Sip_income, GINT_TO_POINTER(digest)) + 1 ); 
        return 1;
    }
 
}

// This function computes the digest for a string based on the HMAC_SHA256 function
int computeDigest(char *line , char *digest)
{
    // Define parameters
    unsigned char obuf[32];
    char *HMAC_Result=NULL, key[]="SCYPE5179_SECRET_KEY";
    int i, digestLen=65;
	
    // Compute HMAC_SHA256
    HMAC(EVP_sha256(), key, strlen(key), (unsigned char *)line, strlen(line), obuf, NULL);
    // Print the 64 digest in an appropriate buffer
    for (i = 0; i < 32; i++) 
    {
        snprintf(digest+i*2, 4, "%02x", obuf[i]);
    }
    return 1;
}








