/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asteriskjava;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Aegean
 */
public class WindowHandler {
    
     public static String[] chop(String redDataText) {
        String lin[] = redDataText.split("\\r?\\n");
        String lines[] = new String[6];

        int j = 0;
        for (int i = 0; i < 7; i++) {
            if (i != 2) {
                lines[j] = lin[i];
                j++;
                //System.out.println(lines[j-1]);
            }
        }
        return lines;
    }

    public static void printMsg(String[] msg) {
        for (int i = 0; i < msg.length; i++) {
            System.out.println(msg[i]);
        }

    }

    public static int[] checkmsg(String[] c, String skey, Hashtable<String, Integer> FirstHashTable ) {
        int[] occ = new int[6];

        for (int i = 0; i < c.length; i++) {

            String digest = hmacDigest(c[i], skey, "HmacSHA256");
            //System.out.println(digest);
            if (FirstHashTable.containsKey(digest)) {
                occ[i] = FirstHashTable.get(digest);
            } else {
                occ[i] = 0;
            }
            System.out.print(occ[i] + ",");

        }
        System.out.println();

        return occ;
    }

    public static void AddToHashTable(String[] msg, String skey, Hashtable<String, Integer> FirstHashTable) {

        for (int i = 0; i < msg.length; i++) {

            String digest = hmacDigest(msg[i], skey, "HmacSHA256");
            //System.out.println(msg[i]);
            //System.out.println(digest);

            // if key exists then we increase the occurences by 1
            // else we add a new header digest in the hashtable
            if (FirstHashTable.containsKey(digest)) {
                //System.out.println("Already in hashtable");
                Integer occurrences = FirstHashTable.get(digest);
                occurrences++;
                FirstHashTable.put(digest, occurrences);

            } else {
                // System.out.println("first time in the list");
                FirstHashTable.put(digest, 1);
            }

            //System.out.println(FirstHashTable.get(digest));
        }

        System.out.println("size : " + FirstHashTable.size());

    }

    public static String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;
    }

    public static String SHA256(String header) {
        try {

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(header.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            //System.out.println("Hex format : " + sb.toString());
            //convert the byte to hex format method 2
//            StringBuffer hexString = new StringBuffer();
//            for (int i=0;i<byteData.length;i++) {
//                String hex=Integer.toHexString(0xff & byteData[i]);
//                if(hex.length()==1) hexString.append('0');
//                hexString.append(hex);
//            }
//            System.out.println("Hex format : " + hexString.toString());
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AsteriskJava.class.getName()).log(Level.SEVERE, null, ex);
            return "unsuccessful SHA256";
        }
    }
    
}
