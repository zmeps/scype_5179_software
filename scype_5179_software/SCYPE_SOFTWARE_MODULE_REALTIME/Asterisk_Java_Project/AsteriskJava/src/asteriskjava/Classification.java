/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asteriskjava;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/**
 *
 * @author Aegean
 */
public class Classification {

    File file = null;
    PrintStream ps = null;
    public static int i = 0;
    //public static Logger logger;
    public static String path;

    public static void train() {
        try {

            // create J48
            Classifier c = new J48();

            // train
            Instances inst = new Instances(
                    new BufferedReader(
                            new FileReader("sipResultFile.arff")));
            inst.setClassIndex(inst.numAttributes() - 1);
            c.buildClassifier(inst);

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("build.model"));
            oos.writeObject(c);
            oos.flush();
            oos.close();

        } catch (Exception ex) {
        }
    }

    public static double test(int[] valArray) {
        double k = 0;
        try {

            Classifier ci = null;
            Instances isTestingSet = null;
            Instance i1 = null;
            Attribute Attribute1 = null;
            Attribute Attribute2 = null;
            Attribute Attribute3 = null;
            Attribute Attribute4 = null;
            Attribute Attribute5 = null;
            Attribute Attribute6 = null;
            FastVector fvWekaAttributes = null;
            PrintWriter oos = null;

            // Declare two numeric attributes
            if ((i++) == 0) {

                Attribute1 = new Attribute("s1");
                Attribute2 = new Attribute("s2");
                Attribute3 = new Attribute("s3");
                Attribute4 = new Attribute("s4");
                Attribute5 = new Attribute("s5");
                Attribute6 = new Attribute("s6");

                FastVector fvClassVal = new FastVector(2);
                fvClassVal.addElement("normal");
                fvClassVal.addElement("attack");
                Attribute ClassAttribute = new Attribute("theClass", fvClassVal);

                // Declare the feature vector
                fvWekaAttributes = new FastVector(7);
                fvWekaAttributes.addElement(Attribute1);
                fvWekaAttributes.addElement(Attribute2);
                fvWekaAttributes.addElement(Attribute3);
                fvWekaAttributes.addElement(Attribute4);
                fvWekaAttributes.addElement(Attribute5);
                fvWekaAttributes.addElement(Attribute6);
                fvWekaAttributes.addElement(ClassAttribute);

                // Create an empty training set
                isTestingSet = new Instances("Rel", fvWekaAttributes, 10);
                // Set class index
                isTestingSet.setClassIndex(6);

                i1 = new Instance(6);
                // load classifier from file
                ObjectInputStream ois = new ObjectInputStream(
                        new FileInputStream("build.model"));
                ci = (Classifier) ois.readObject();
                ois.close();

                oos = new PrintWriter("results.txt");
            }

            i1.setValue(Attribute1, valArray[0]);
            i1.setValue(Attribute2, valArray[1]);
            i1.setValue(Attribute3, valArray[2]);
            i1.setValue(Attribute4, valArray[3]);
            i1.setValue(Attribute5, valArray[4]);
            i1.setValue(Attribute6, valArray[5]);
            // add the instance
            isTestingSet.add(i1);

            k = ci.classifyInstance(isTestingSet.instance(0));

        } catch (Throwable e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();

        }
        return k;
    }
}
