/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asteriskjava;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class AsteriskJava {

    //message window
    private static final int Mw = 100;

    private static Hashtable<String, Integer> FirstHashTable
            = new Hashtable<String, Integer>();

    private static Hashtable<String, Integer> SecondHashTable
            = new Hashtable<String, Integer>();

    private static final String skey = "a symmetric key";

    /*for counting the invite messages and finding out when the
     first Mw is completed. When the first Mw is completed is used to
     count messages until 2*Mw limit is reached. 
     */
    private static int mcount = 0;

    /*
     global counter for counting the messages.
     It can be deleted after code testing.
     */
    private static int gcount = 0;

    public static void main(String[] args) throws IOException {

        String serverip = "195.251.166.103";
        Socket s = new Socket(serverip, 5000);
        BufferedReader input;

        int red = -1;
        byte[] buffer = new byte[5 * 1024]; // a read buffer of 5KiB
        byte[] redData;
        StringBuilder clientData = new StringBuilder();
        String redDataText;
        
        Classification.train();
        
        

        while ((red = s.getInputStream().read(buffer)) > -1) {
            redData = new byte[red];
            System.arraycopy(buffer, 0, redData, 0, red);
            redDataText = new String(redData, "UTF-8"); // assumption that client sends data UTF-8 encoded

            if (redDataText.contains("INVITE sip") && !redDataText.contains("ACK sip") && !redDataText.contains("404 Not Found") && !redDataText.contains("Authorization:")) {
                String[] chop = WindowHandler.chop(redDataText);
                gcount++;
                System.out.println("======MSG==========" + gcount);
                WindowHandler.printMsg(chop);
                System.out.println("===================");
                //redDataText = "";
                //System.out.println("message part recieved:" + redDataText);
                mcount++;

                /*
                 elegxos gia na gemisei mia fora to parathuro
                 oso erxontai mhnymata apla gemizoume to 1o hashtable
                 */
                if (mcount <= Mw) {
                    System.out.println("mcount < Mw ======");
                    WindowHandler.AddToHashTable(chop,skey,FirstHashTable);
                } else {
                    /*
                     an exei gemisei to prwto parathuro tote vriskoume ton arithmo
                     emfanisewn twn headers tou epomenou mhnymatos kai to prosthetoume sugxrows
                     sthn 2h hashtable.
                     kanoume to minima classify.
                    
                     */
                    int[] checkmsg = WindowHandler.checkmsg(chop,skey,FirstHashTable);
                    WindowHandler.AddToHashTable(chop,skey,SecondHashTable);
                    System.out.println("classify: ========= : "+Classification.test(checkmsg));
                   
                    /*
                     otan ta minimata ginoun 2 fores to Mw tote o prwtos hashtable adeiazei
                     kai gemizei me ton deutero pou periexei ta dedomena gia to 2o Mw.
                     o 2os hashtable adeiazei kai gemizei me to 3o Mw k.o.k.
                     */
                    if (mcount == 2 * Mw) {
                        System.out.println("===========  2*Mw");
                        FirstHashTable.clear();
                        FirstHashTable.putAll(SecondHashTable);
                        SecondHashTable.clear();
                        mcount = Mw;
                    }
                }

            }
        }
//        String S = "INVITE sip:6@195.251.166.103 SIP/2.0\n"
//                + "Via: SIP/2.0/UDP 195.251.166.84:41384;rport;branch=z9hG4bKPjt9abLdntcmu8b-wi5kOJwy4KyCKUUSxM\n"
//                + "Max-Forwards: 70\n"
//                + "From: \"bob\" <sip:bob@195.251.166.103>;tag=1zI9ZTXUn3OKHSqpMO3lmJGCZNSkCUnd\n"
//                + "To: <sip:6@195.251.166.103>\n"
//                + "Contact: \"bob\" <sip:bob@195.251.166.84:41384;ob>\n"
//                + "Call-ID: 7fG98X0wMmeUlWFM4JuyrxmsdhenNRN0";
//
//        chop(S);

    }

   
}
