/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include "mainwindow.h"
#include <QApplication>
#include <stdio.h>


void clean_myfile() {
    remove( "C:\\sqlite\\scype_5179.db" );
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    w.setWindowIcon(QIcon("C:/Users/icsd-tzisis/Desktop/header.png"));
    //w.setFixedSize();
    std::atexit( clean_myfile );
    return a.exec();
}
