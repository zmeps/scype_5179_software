/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#include <stdio.h>
#include <glib.h>

#include "readWrite.h"
#include "entropyMetrics.h"
#include "encryptLogFile.h"
#include "constants.h"
#include "headerFunctions.h"
// ---------------------------------------------------------------------

// Declare constant symbols
const char *Os[] = {"First Line - Os1 : ", "Os2 : ", "Os3 : ", "Os4 : ", "Os5 : ", "Os6 : ", "Os7: "};
const char *Hs[] = {"Hs1 : ", "Hs2 : ", "Hs3 : ", "Hs4 : ", "Hs5 : ", "Hs6 : ", "Hs7 : "};

// Read Log File
int readLogFile(int choice)
{
    // Allocate the hash table
    GHashTable* SipHashTable = g_hash_table_new(g_str_hash, g_str_equal);

    // Begin printing in the output file
    FILE *openSipFile=NULL, *finalOutputFile = fopen(outputFile,"w+");
    fprintf(finalOutputFile, "\n\n  Msg,    InfS1,    InfS2,    InfS3,    InfS4,    InfS5,    InfS6,    Infs7,    Actual Info\n");
    fprintf(finalOutputFile, "***********************************************************************************************");
    fclose(finalOutputFile);

    char line[160], *encrypted=NULL;
    int currentSymbol=0, totalMessages=0, round=0, realMessages=0;
    static int isFirst=0, startLine=0, endLine=0;

    openSipFile = fopen(inputFile, "r");

    // Get file line, while it hasnt reached EOF
    while(fgets(line, 160, openSipFile)!=NULL)
    {
        // If the messages exceed the message window
        if(totalMessages>=MESSAGE_WINDOW)
        {
             startLine = MESSAGE_WINDOW*(round++);
             endLine = MESSAGE_WINDOW*round;
             printOccurences(choice, startLine, endLine, SipHashTable);
             // destroy the hash table and allocate a new one
             g_hash_table_destroy(SipHashTable);
             SipHashTable = g_hash_table_new(g_str_hash, g_str_equal);
             totalMessages=0;
        }
        // If the current line contains a valid symbol
        if(currentSymbol = headersToSipMessage(line))
        {
             // If it is the last symbol of the message increase the number of messages
             if(currentSymbol==333 || currentSymbol==666)
             {
                 totalMessages++;
                 realMessages++;
             }
             // Encrypt the line and return a pointer to the digest
             encrypted = encryptHeaderHMAC_SHA256(line);
             // Insert the digest to the hash table
             insertToHashTable(SipHashTable, encrypted);
             // Free allocated space
             free(encrypted);
        }
    }
    // When the log file reading ends if there are messages remaining from the final
    // round then proceed with these messages
    if(totalMessages%MESSAGE_WINDOW!=0)
    {
        startLine = endLine;
        endLine = realMessages;
        printOccurences(choice, startLine, endLine, SipHashTable);
        g_hash_table_destroy(SipHashTable);
    }
    fclose(openSipFile);
    //g_hash_table_destroy(SipHashTable);
    return 1;
}
// -------------------------------
// This function prints occurences for every SIP message included in a different file per message window
int printOccurences(int choice, int startLine, int endLine, GHashTable* SipHashTable)
{
    ////////////////////////////////////////////////////////////////////////
    FILE *resultFile=NULL;
    FILE *openSipFile=NULL;
    char line[160], *encrypted=NULL;
    char temporary[120];

    static int rounds=0;
    int currentSymbol=0, totalMessages=0, digestLen=65, numberOfOccurences=0, isAttack=0;
    static int startMessage=0, endMessage=0, realMessages=0;
    ////////////////////////////////////////////////////////////////////////
    startMessage=startLine;
    endMessage=endLine;
    rounds++;
    // We should write the results in a new file every time according to the round
    returnConcatenatedPath(choice, rounds, temporary);

    resultFile = fopen(temporary,"w+");
    fclose(resultFile);

    openSipFile = fopen(inputFile, "r");

    while(fgets(line, 160, openSipFile)!=NULL)
    {
        if(currentSymbol = headersToSipMessage(line))
        {
             if(totalMessages>=startMessage && totalMessages<=endMessage)
             {
                   encrypted = encryptHeaderHMAC_SHA256(line);
                   numberOfOccurences = hashTableValue(SipHashTable, encrypted);
                   isAttack = isAttackMessage(line);
                   computeInformation(currentSymbol, numberOfOccurences);

                   if(currentSymbol==333)
                   {
                       resultFile = fopen(temporary, "a+");
                       if(choice==ENTROPY_)
                       {
                           fprintf(resultFile,"\n%s%s",  Os[4],line);
                           fprintf(resultFile, "++%s", Hs[4]);
                           fprintf(resultFile, "%s", encrypted);
                           fprintf(resultFile, "\n$$%d%sAp##%d      ",totalMessages+1 , Hs[4], numberOfOccurences);
                           fprintf(resultFile, "\n--%dHs6      \n",totalMessages+1);
                       }
                       else if(choice==MACHINE_LEARNING_)
                       {
                           if(isAttack)
                           {
                               fprintf(resultFile, "0, attack\n");
                           }
                           else
                           {
                               fprintf(resultFile, "0, normal\n");
                           }
                       }
                       fclose(resultFile);
                   }
                   else if(currentSymbol==666)
                   {
                       resultFile = fopen(temporary, "a+");
                       if(choice==ENTROPY_)
                       {
                           fprintf(resultFile,"\n%s%s",  Os[5],line);
                           fprintf(resultFile, "++%s", Hs[5]);
                           fprintf(resultFile, "%s", encrypted);
                           fprintf(resultFile, "\n$$%d%sAp##%d      ",totalMessages+1 , Hs[5], numberOfOccurences);
                           fprintf(resultFile, "\n--%dHs7      \n",totalMessages+1);
                       }
                       else if(choice == MACHINE_LEARNING_)
                       {
                           if(isAttack)
                           {
                               fprintf(resultFile, "%d, attack\n", numberOfOccurences);
                           }
                           else
                           {
                               fprintf(resultFile, "%d, normal\n", numberOfOccurences);
                           }
                       }
                       fclose(resultFile);
                   }
                   else
                   {
                           resultFile = fopen(temporary, "a+");
                           if(choice==ENTROPY_)
                           {
                                fprintf(resultFile,"\n%s%s",  Os[currentSymbol-1],line);
                                fprintf(resultFile, "++%s", Hs[currentSymbol-1]);
                                fprintf(resultFile, "%s", encrypted);
                                fprintf(resultFile, "\n$$%d%sAp##%d      \n",totalMessages+1 , Hs[currentSymbol-1], numberOfOccurences);
                           }
                           else if(choice==MACHINE_LEARNING_)
                           {
                                resultFile = fopen(temporary, "a+");
                                fprintf(resultFile, "%d, ", numberOfOccurences);
                           }
                           fclose(resultFile);
                   }
                   free(encrypted);
             }
             if(currentSymbol==333 || currentSymbol==666)
             {
                       totalMessages++;
                       realMessages++;
                       if(totalMessages==endMessage)
                       {
                            return 1;
                       }
             }
        }
    }
    fclose(openSipFile);
    g_hash_table_destroy(SipHashTable);
    return 1;
}
