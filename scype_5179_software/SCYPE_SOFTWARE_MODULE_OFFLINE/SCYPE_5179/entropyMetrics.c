/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "constants.h"

float entropyArray[6]={0.0, 0.0, 0.0 , 0.0 , 0.0 , 0.0};
float SUM=0.0;

FILE *resultFile=NULL, *outputEntropyFile=NULL, *tmpResultFile=NULL;

const char *originalSymbols[] = {"","InfS1 : ", "InfS2 : ", "InfS3 : ", "InfS4 : ", "InfS5 : ", "InfS6 : "};
const char *entropySymbols[] = {" S1 - First Line : ", " S2 - Via : ", " S3 - From : ", " S4 - To : ", " S5 - Call-ID : ", " S6 - Contact : ", " S7 - Entire Sip Message : "};
const char *symbols[] = {"","Hs1 : ", "Hs2 : ", "Hs3 : ", "Hs4 : ", "Hs5 : ", "Hs6 : "};


//////////////////////////////////////////////////////////////////
void computeInformation(int currentSymbol, int numberOfOccurences)
{


          static int numOfMessages=0;
          static float actualInfo=0.0, meanValue=0.0;
          float temp=0.0, temp1=0.0, *variance = NULL;

          // Allocate memory to store the Actual Info Values
          variance = (float *)malloc(MESSAGE_WINDOW * sizeof(float));

          if(!variance)
          {
               printf("\nMemory couldn't be allocate for Actual Info values... Exiting !\n");
               exit(0);
          }

          //////////////////////////////////////////////////////////////////
          temp = -log2((float)numberOfOccurences/MESSAGE_WINDOW);
          temp1 = -log2((float)1/MESSAGE_WINDOW);
          actualInfo+=temp;

          outputEntropyFile = fopen(outputFile, "a");
          // Open first stage analysis file for reading
          if(currentSymbol==1)
          {
                fprintf(outputEntropyFile, "\n\n   %d,    %.2f,", numOfMessages+1, temp); // printf number of messages in the final fil
                //entropyComputation(currentSymbol,temp);
          }
          else if(currentSymbol==333)
          {
                actualInfo+=temp1;
                meanValue+=actualInfo;
                variance[numOfMessages%MESSAGE_WINDOW]=actualInfo;
                fprintf(outputEntropyFile, "     %.2f,    <-->,     %.2f,     %.2f", temp, temp1, actualInfo);
                //entropyComputation(5,temp);
                //entropyComputation(7,temp1);
                numOfMessages++;
                actualInfo=0.0;

          }
          else if(currentSymbol==666)
          {
                actualInfo+=temp1;
                meanValue+=actualInfo;
                variance[numOfMessages%MESSAGE_WINDOW]=actualInfo;
                fprintf(outputEntropyFile, "     %.2f,     %.2f,    %.2f", temp, temp1, actualInfo);
                //entropyComputation(6,temp);
                //entropyComputation(7,temp1);
                numOfMessages++;
                actualInfo=0.0;
          }
          else
          {
                //entropyComputation(currentSymbol,temp);
                fprintf(outputEntropyFile, "     %.2f,", temp);
          }
          fclose(outputEntropyFile);
          free(variance);
          /*if(numOfMessages==MESSAGE_WINDOW)
          {
                printEntropyResults(meanValue, variance, );
          }
          */
}

//////////////////////////////////////////////////////////////////////////////////
void entropyComputation(int symbolCounter, float temp)
{
    double entropyTmp = 0.0;

    //entropyTmp = temp*probability;
    entropyArray[symbolCounter-1]+= entropyTmp;
}
