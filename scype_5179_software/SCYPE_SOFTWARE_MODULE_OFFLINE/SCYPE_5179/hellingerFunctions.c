/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "hellingerFunctions.h"
#include "headerFunctions.h"
#include "constants.h"

void mainHellingerFunction(int choice)
{
     int i=0, j=0;
     // We declare a 2-dimension array in order to save
     // the values of occurences per different types of messages (INVITE, ACK, BYE)//
     float A[ROWS][COLS]= {0.0};
     char temporary[120];
     rounds =0;
     // We should normalize the mean values of occurences
     outputPath = outputTrainPath;
     /* The first loop reads the already processed files to
     extract and compute the mean values of occurences for INVITE messages per header*/
     zeroHellingerValues();
     for(i=0; i<BLOCK1; i++)
     {
         rounds++;
         returnConcatenatedPath(choice, i+1, temporary);
         readSipMessagesOccurences(TRAIN_FILE, temporary, A);
     }
         
     // We should normalize the mean values of occurences
     normalize2DTable(A);
      
     if(choice == HELLINGER_DISTANCE_1)
     {
         print2DTable(A);
         outputPath = outputTestPath;
     }

     // The second loop reads the already processed files to
     // extract the occurences and compute the hellinger distance value from the previous mentioned average values
     hellingerAvgInv=0;
     do{

         rounds=0;
         numOfInvites=0;
         numOfAcks=0;
         numOfByes=0;
         varianceInv=0;
         hellingerInv=0;
         FNInv=0;
         FPInv=0;
         TPInv=0;

         for(i=0; i<BLOCK2; i++)
         {
            returnConcatenatedPath(HELLINGER_DISTANCE_1, i+1, temporary);
            readSipMessagesOccurences(TEST_FILE, temporary, A);
         }
         trueHellingerAvgInv = hellingerAvgInv/numOfInvites;

         printf("\nAVG HELLINGER : [%f]", trueHellingerAvgInv);
         printf("\nDIVISION OF : [%f], [%d]", hellingerAvgInv, numOfInvites);
         printf("\nVARIANCE : [%f]", varianceInv/numOfInvites);
         printf("\nSTANDARD DEVIATION : [%f]", sqrt(varianceInv/numOfInvites));
         printf("\nTOTAL INVITE MESSAGES : %d", numOfInvites);
         printf("\nTOTAL ACK MESSAGES : %d", numOfAcks);
         printf("\nTOTAL BYE MESSAGES : %d", numOfByes);
         printf("\nSUM OF ALL MESSAGES : %d", numOfInvites+numOfAcks+numOfByes);
         printf("\n\nINVITE HELLINGER ATTACK MESSAGES : [%d]", hellingerAttackMessagesInv);
         printf("\nFN MESSAGES : %d", FNInv);
         printf("\nFP MESSAGES %d", FPInv);

         printf("\n============================\nSECOND ROUND\n=======================\n");
         if(choice==HELLINGER_DISTANCE_1)
             break;
     }while((++j) < 2);
}

void readSipMessagesOccurences(int typeOfFile, char *file, float normalMessage[][COLS])
{
     // -------------------------------------------------------------
     FILE *openSipFile=NULL;
     char *tmp=NULL, *tmp1=NULL;
     char line[160];
     float *attackMessage;
     int occurences=0, x=0, symbolCounter=0, currentSymbol=0, i=0, j=0, currentSymbolAndOccurences[2];
     int flagInvite=0, flagAck=0, flagBye=0, attackHeader=0, attackFlag=0;

     attackMessage = (float *) malloc(ROWS*sizeof(float));
     attackMessage[0] = 0.0;
     attackMessage[0] = 0.0;
     attackMessage[0] = 0.0;
     attackMessage[0] = 0.0;
     attackMessage[0] = 0.0;
     attackMessage[0] = 0.0;
     // Trafic file
     openSipFile = fopen(file, "r");
     // ------------------------------------
     while(fgets(line, 160, openSipFile)!=NULL)
     {
             // If the current line contains a valid symbol proceed
             if(currentSymbol = headersToSipMessage(line))
             {
                   // If the current line corresponds to an INVITE first line header
                   if(strstr(line,"INVITE"))
                   {
                        // Activate an appropriate flag
                        flagInvite=1;
                        numOfInvites++;
                        // Count the number of invites
                        if(typeOfFile==TEST_FILE)
                        {

                            //printf("\n Oarithmos twn invites tha eina : %d", numOfInvites);
                        }
                   }
                   else if(strstr(line,"ACK"))
                   {
                        flagAck=1;
                        numOfAcks++;
                   }
                   else if(strstr(line,"BYE"))
                   {
                        flagBye=1;
                        numOfByes++;
                   }
                   else if(strstr(line, ":333"))
                   {
                        attackFlag=1;
                        attackMessages++;
                   }
                   else if(strstr(line, ":9"))
                   {
                         attackFlag=1;
                         attackMessages++;
                   }
                   if(currentSymbol==333)
                   {

                        if(typeOfFile==TRAIN_FILE)
                        {
                            normalMessage[5][1]=0;
                            normalMessage[5][2]=0;
                        }
                        else
                        {
                            attackMessage[5]=0;
                        }
						
                        flagInvite=0, flagAck=0, flagBye=0, attackFlag=0;
                   }
             }
             else if(strstr(line,"$$"))
             {
                   extractNumberFromString(line, currentSymbolAndOccurences);

                   if(flagInvite)
                   {
                        if(typeOfFile==TRAIN_FILE)
                        {
                           normalMessage[currentSymbolAndOccurences[0]-1][0] +=((float)currentSymbolAndOccurences[1]);
                        }
                        else if(typeOfFile==TEST_FILE)
                        {
                           attackMessage[currentSymbolAndOccurences[0]-1] +=((float)currentSymbolAndOccurences[1]);
                        }
                   }
                   // Code must be written for the other two cases {ACK, BYE}
             }
             else if(strstr(line, "--"))
             {
                   if(typeOfFile==TEST_FILE)
                   {
                         normalizeTable(attackMessage);
                         if(flagInvite==1)
                         {
                             hellingerInv = hellingerDistance(INVITE, attackMessage, normalMessage);
                             varianceInv+= pow((hellingerInv-trueHellingerAvgInv), 2);

                             // DETECTION...
                             if( (hellingerInv>hellingerInvDetectionThreshold) )
                             {

                                 hellingerAttackMessagesInv++;
                                 if(attackFlag==1)
                                 {
                                     TPInv++;
                                 }
                                 else{
                                     FPInv++;
                                 }
                             }
                             else
                             {
                                 if(attackFlag==1)
                                 {
                                    FNInv++;
                                 }
                             }

                             hellingerAvgInv+=hellingerInv;
                             totalMessagesInv++;
                         }
                    }

             }
     }

     if(rounds == BLOCK1)
     {
        computeMeanValues2DTable(normalMessage, numOfInvites);
     }

     fclose(openSipFile);
     free(attackMessage);
}

int checkForAttackHeader(char *line)
{
    char *tmp=NULL, tmp1=NULL;
        // If a message comes from one of these ports then it is an attack message
        tmp = strstr(line,":9");
        tmp1 = strstr(line,":333");

        if(!tmp || !tmp1)
        {
            return 1;
        }
    return 0;
}

void computeMeanValues2DTable(float K[][COLS], int normalInvites)
{
     int i,j;
     for(i=0; i<6; i++)
        for(j=0; j<3; j++)
        {
             if(j==0)
             {
                K[i][j]=ceil(K[i][j]/normalInvites);
             }
             //else if(j==1)
                //K[i][j]=ceil(K[i][j]/normalAcks);
             //else if(j==2)
                //K[i][j]=ceil(K[i][j]/normalByes);
        }
}

void normalize2DTable(float K[][COLS])
{
     int i=0, j=0;
     float SUM1=0, SUM2=0, SUM3=0;

     for(i=0; i<6; i++)
     {
        for(j=0; j<3; j++)
        {
           if(j==0)
             SUM1+= K[i][j];
           if(j==1)
             SUM2+= K[i][j];
           if(j==2)
             SUM3+= K[i][j];
        }
     }

     for(i=0; i<6; i++)
     {
        for(j=0; j<3; j++)
        {
            if(j==0)
            {
                K[i][j]= K[i][j]/SUM1;
            }
            else if(j==1)
            {
                K[i][j]= K[i][j]/SUM2;
            }
            else if(j==2)
            {
                K[i][j]= K[i][j]/SUM3;
            }

        }
     }
}

void printTable(float K[])
{
     int i;
     for(i=0; i<6; i++)
     {
             printf("\nPRINT 1D %f", K[i]);
     }
}

void print2DTable(float K[][COLS])
{
     int i, j;
     for(i=0; i<6; i++)
     {
        for(j=0; j<3; j++)
        {
             printf("\nPRINT 2D %f", K[i][j]);
        }
     }
}

float hellingerDistance(int messageType, float A[], float B[][COLS])
{
      int i=0, j=0;
      float hellinger=0.0;

      for(i=0; i<6; i++)
      {
            if(messageType==INVITE)
            {
                 hellinger+= pow(sqrt(A[i]) - sqrt(B[i][0]), 2);
                 //continue;
            }/*
            else if(messageType==ACK)
               hellinger+= pow(sqrt(A[i]) - sqrt(B[i][1]), 2);
            else if(messageType==BYE)
               hellinger+= pow(sqrt(A[i]) - sqrt(B[i][2]), 2);*/
      }
      return hellinger*0.5;
}

void normalizeTable(float *K)
{
     int i=0;
     float SUM=0.0;

     for(i=0; i<6; i++)
     {
        SUM+= K[i];
     }
     for(i=0; i<6; i++)
     {
        K[i]=K[i]/SUM;
     }
}

void zeroHellingerValues()
{

     numOfInvites=0, numOfAcks=0, numOfByes=0;
     hellingerAttackMessagesInv=0, attackMessages=0, TPInv=0, FPInv=0, FNInv=0, attackMessagesInv=0, totalMessagesInv=0;
     hellingerAttackMessagesAck=0, TPAck=0, FPAck=0, FNAck=0, attackMessagesAck=0, totalMessagesAck=0;
     hellingerAttackMessagesBye=0, TPBye=0, FPBye=0, FNBye=0, attackMessagesBye=0, totalMessagesBye=0;
     hellingerInv=0.0, varianceInv=0.0, stDeviationInv=0.0, hellingerAvgInv=0.0;
     hellingerAck=0.0, varianceAck=0.0, stDeviationAck=0.0, hellingerAvgAck=0.0;
     hellingerBye=0.0, varianceBye=0.0, stDeviationBye=0.0, hellingerAvgBye=0.0;
}
