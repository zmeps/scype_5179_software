/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <stdio.h>
#include <glib.h>
#include "constants.h"

// Define the appropriate directories for input and output files
char *inputFile=NULL;
char *sipTrainFile=NULL;
char *sipTestFile=NULL;
char *sipTrainEntropyMetricsFile=NULL;
char *sipTestEntropyMetricsFile=NULL;
char *outputFile=NULL;
char *outputTrainPath=NULL;
char *outputTestPath=NULL;
char *outputPath=NULL;
char *testFilePath=NULL;

// Entropy variables...
float entropyDetectionThreshold=-1;
int entropyFP=0, entropyFN=0, entropyTP=0, entropyAttackMessages=0;

int numOfFiles=0;
int _train=0, _test=0;
int rounds=0;
int MESSAGE_WINDOW=-1;


// Hellinger variables...
int BLOCK1=0, BLOCK2=0;

int numOfInvites=0, numOfAcks=0, numOfByes=0;
int hellingerAttackMessagesInv=0, attackMessages=0, TPInv=0, FPInv=0, FNInv=0, attackMessagesInv=0, totalMessagesInv=0;
int hellingerAttackMessagesAck=0, TPAck=0, FPAck=0, FNAck=0, attackMessagesAck=0, totalMessagesAck=0;
int hellingerAttackMessagesBye=0, TPBye=0, FPBye=0, FNBye=0, attackMessagesBye=0, totalMessagesBye=0;
float hellingerInv=0.0, varianceInv=0.0, stDeviationInv=0.0, hellingerAvgInv=0.0, trueHellingerAvgInv=0.0;
float hellingerAck=0.0, varianceAck=0.0, stDeviationAck=0.0, hellingerAvgAck=0.0;
float hellingerBye=0.0, varianceBye=0.0, stDeviationBye=0.0, hellingerAvgBye=0.0;
float hellingerInvDetectionThreshold=0.0;

