/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#ifndef READWRITE_H
#define READWRITE_H

#include <glib.h>

// Function Prototypes
int readLogFile(int);
int printOccurences(int, int, int, GHashTable*);
void writeEntropyHellingerParts(int);
void returnConcatenatedPath(int, int, char *);

#endif // READWRITE_H_INCLUDED
