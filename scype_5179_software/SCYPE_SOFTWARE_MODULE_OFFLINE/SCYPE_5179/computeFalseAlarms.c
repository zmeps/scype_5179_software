/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <stdio.h>
#include "constants.h"
#include "computeFalseAlarms.h"

void computeEntropyFalseAlarms()
{

    char temp[100], line[10];
    int entropyAttackMessage=0, attackFlag=0, realAttackMessages=0;

    FILE *entropyMessageFile=NULL, *realAttackMessageFile=NULL;

    strcpy(temp, outputTestPath);
    strcat(temp, "attackMessages.txt");
    entropyMessageFile = fopen(temp, "r");

    strcpy(temp, outputTestPath);
    strcat(temp, "realAttackMessages.txt");
    realAttackMessageFile = fopen(temp, "r");

    while(fgets(line, 160, entropyMessageFile))
    {

        entropyAttackMessage = atoi(line);
        while(fgets(line, 160, realAttackMessageFile))
        {
            realAttackMessages++;
            if(entropyAttackMessage == atoi(line))
            {
                entropyTP++;
                attackFlag=1;

                break;
            }

        }

        if(!attackFlag)
        {
            entropyFP++;
        }
        else
        {
            attackFlag=0;
        }
    }

    entropyFN = realAttackMessages - entropyTP;
    fclose(entropyMessageFile);
    fclose(realAttackMessageFile);

}
