/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <glib.h>

#ifndef CONSTANTS_H
#define CONSTANTS_H

// Available methods
#define ENTROPY_ 1
#define HELLINGER_DISTANCE_ 2
#define HELLINGER_DISTANCE_1 12
#define MACHINE_LEARNING_ 3
#define FALSE_ALARMS 4

// Hellinger Distance constants
//#define BLOCK1 96
//#define BLOCK2 96

// Define thresholds for every type of message.
// This constant will be equal to mean value plus the standard deviation
#define INV_THRESHOLD 026
#define INV_THRESHOLD_NEG 004
#define ACK_THRESHOLD ?
#define BYE_THRESHOLD ?

// The average value of all distances for different types of messages
#define AVG_HELLINGER_INVITE 034
#define AVG_HELLINGER_ACK 01
#define AVG_HELLINGER_BYE 01

#define ROWS 6
#define COLS 3


// Select the message window
extern int MESSAGE_WINDOW;

extern int _train, _test;

extern char *inputFile;
extern char *sipTrainFile;
extern char *sipTestFile;
extern char *sipTrainEntropyMetricsFile;
extern char *sipTestEntropyMetricsFile;
extern char *outputFile;
extern char *outputTrainPath;
extern char *outputTestPath;
extern char *outputPath;
extern char *testFilePath;

extern int rounds;
extern int numOfFiles;

// Entropy variables
extern int entropyFP, entropyFN, entropyTP, entropyAttackMessages;
extern GHashTable* attackMessagesHash;
extern float entropyDetectionThreshold;

// Hellinger Distance Variables

extern int BLOCK1, BLOCK2;

extern int numOfInvites, numOfAcks, numOfByes;
extern int hellingerAttackMessagesInv, attackMessages, TPInv, FPInv, FNInv, attackMessagesInv, totalMessagesInv;
extern int hellingerAttackMessagesAck, TPAck, FPAck, FNAck, attackMessagesAck, totalMessagesAck;
extern int hellingerAttackMessagesBye, TPBye, FPBye, FNBye, attackMessagesBye, totalMessagesBye;
extern float hellingerInv, varianceInv, stDeviationInv, hellingerAvgInv, trueHellingerAvgInv;
extern float hellingerAck, varianceAck, stDeviationAck, hellingerAvgAck;
extern float hellingerBye, varianceBye, stDeviationBye, hellingerAvgBye;
extern float hellingerInvDetectionThreshold;

/////////////////////////////////////
enum FILETYPE{TRAIN_FILE, TEST_FILE};
enum MESSAGES{INVITE, ACK, BYE};

#endif // CONSTANTS_H_INCLUDED
