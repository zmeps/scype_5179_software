/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "constants.h"

void mergeFilesInDirectory()
{

    FILE *fs1, *fs2;
    char temp[60], *temporary, k;

    char ch;
    int i=0;
    numOfFiles = rounds;

    outputFile = (char *) malloc (sizeof(char)* strlen(outputPath)+50);

    strcpy(outputFile, outputPath);
    strcat(outputFile, "sipResultFile.arff");

    for(i=1; i<=numOfFiles; i++)
    {
           temporary = (char *) malloc(sizeof(char) *100);
           strcpy(temporary, outputPath);
           fs2 = fopen(outputFile,"a+");
           sprintf(temp,"%d",i);
           strcat(temporary,temp);
           strcat(temporary,".arff");
           printf("\n%s\n", temporary);
           fs1 = fopen(temporary,"r");

           if(fs1==NULL)
           {
              perror("Error ");
              printf("Press any key to exit...\n");
              getch();
              exit(EXIT_FAILURE);
           }
           else
           {
              while((ch=fgetc(fs1))!=EOF)
              {
                  fputc(ch,fs2);
              }
           }

           fclose(fs1);
           fclose(fs2);
           free(temporary);
   }
}
