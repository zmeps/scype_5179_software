/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <openssl/hmac.h>
#include <openssl/sha.h>

//#include "encryptLogFile.h"

// Computes HMAC of a string
char * encryptHeaderHMAC_SHA256(char *line)
{
     unsigned char obuf[32];
     char *HMAC_Result=NULL, key[]="SCYPE5179_SECRET_KEY", *digest=NULL;
     int i, digestLen=65;
     // Allocate memory to store the digest
     digest = (char *)malloc(digestLen*sizeof(char));
     // Compute HMAC_SHA256
     HMAC(EVP_sha256(), key, strlen(key), (unsigned char *)line, strlen(line), obuf, NULL);
     // Print the 64 digest in an appropriate buffer
     for (i = 0; i < 32; i++) {
         snprintf(digest+i*2, 4, "%02x", obuf[i]);
     }
     return digest;
}
