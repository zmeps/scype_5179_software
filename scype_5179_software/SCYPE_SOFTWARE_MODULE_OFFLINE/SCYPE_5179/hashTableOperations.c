/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>


#include "hashTableOperations.h"

int insertToHashTable(GHashTable *SipHash, char *hashedHeader)
{
    char *digest=NULL;
    digest = strdup(hashedHeader);
    // Look up the digest in the hash table. If it has not already been inserted
    if(GPOINTER_TO_INT(g_hash_table_lookup(SipHash, digest)==0))
    {
        // Set the value for this key equal to 1
        g_hash_table_insert(SipHash, digest, GINT_TO_POINTER(1));
        return 1;
    }
    else
    {
        // If the digest already exists in the hash table then increment it by one
        g_hash_table_insert(SipHash, digest, g_hash_table_lookup(SipHash, GINT_TO_POINTER(digest)) + 1 );
        return 1;
    }
    return 0;
}

// Returns the value of a specific key, included in a hash table
int hashTableValue(GHashTable *SipHash, char *hashedHeader)
{
    char *digest=NULL;
    digest = strdup(hashedHeader);
    return GPOINTER_TO_INT(g_hash_table_lookup(SipHash, digest));
}

