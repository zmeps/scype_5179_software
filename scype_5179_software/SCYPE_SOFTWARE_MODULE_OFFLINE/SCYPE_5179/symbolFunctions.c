#include <stdio.h>

#include "symbolFunctions.h"

// Return pointer for appropriate symbol
int findHeader(char line[160], int *currentSymbol)
{
    /*
      - INVITE: Let invite a user or a service to a new session or to modify parameters of a established session.
      - ACK: Confirm the session establishment
      - OPTION: Request information about the capabilities of a server
      - BYE: End of a session
      - CANCEL: Cancel a pending request.
      - REGISTER: Register the user agent.
    */

    static int flagFrstLine=1;
    static int scndVia=0;
    static int sumofHeaders=0;
    char *s=NULL;

        s = strstr(line, "INVITE sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 4;
            return sumofHeaders;
        }
        s = strstr(line, "ACK sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=5;
            //return 4;
            return sumofHeaders;
        }
        s = strstr(line, "BYE sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=5;
            //return 4;
            return sumofHeaders;
        }
        s = strstr(line, "CANCEL sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 5;
            return sumofHeaders;
        }
        s = strstr(line, "OPTIONS sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 5;
            return sumofHeaders;
        }
        s = strstr(line, "REGISTER sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 5;
            return sumofHeaders;
        }

        s = strstr(line, "Via:");

        if( s!= NULL)
        {
            // Compute only the first Via message...
            if((++scndVia) == 1)
            {
                *currentSymbol = 2;
                sumofHeaders--;
                //return 1;
                return sumofHeaders;
            }
            s = NULL;
        }
        s = strstr(line, "From:");
        if( s!= NULL)
        {
            *currentSymbol = 3;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
        s = strstr(line, "To:");
        if( s!= NULL)
        {
            *currentSymbol = 4;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
        s = strstr(line, "Call-ID:");
        if( s!= NULL)
        {
            *currentSymbol = 5;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
        s = strstr(line, "Contact:");
        if( s!= NULL)
        {
            *currentSymbol = 6;
            //flagFrstLine = 1;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
    return 0;
}

int headersToSipMessage(char *line)
{
    int currentSymbol, isAckBye=0, numOfMessages=0, sumHeaders=0;
    static int symbolCounter=0;
    if(findHeader(line, &currentSymbol) > 0)
    {
          if((++symbolCounter==6 || (isAckBye = isAckByeMessage(line, symbolCounter))))
          {
                symbolCounter=0; // Set the initial value to the appropriate counters
                if(isAckBye)
                {
                    return 333;
                }
                else
                {
                    return 666;
                }
          }
          return currentSymbol;
    }
    return 0;
}

int isAckByeMessage(char *line, int symbolCounter)
{
   static int valid=0;

   if(strstr(line, "ACK sip:")!=NULL)
   {
       valid = 1;
   }
   else if(strstr(line, "BYE sip:")!=NULL)
   {
       valid = 1;
   }

   if(valid==1 && symbolCounter==5)
   {
       valid = 0;
       return 1;
   }
   else
       return 0;
}

int isAttackMessage(char *line)
{
    if(strstr(line, ":333") || strstr(line, ":3"))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
