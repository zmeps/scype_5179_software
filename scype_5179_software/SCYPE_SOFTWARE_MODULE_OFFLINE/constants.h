/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

// Select the message window constant
#define MESSAGE_WINDOW 1000

// Available methods
#define ENTROPY_ 1
#define HELLINGER_DISTANCE_ 2
#define HELLINGER_DISTANCE_1 12
#define MACHINE_LEARNING_ 3

// Hellinger Distance constants
#define BLOCK1 96
#define BLOCK2 96

// Define thresholds for every type of message.
// This constant will be equal to mean value plus the standard deviation
#define INV_THRESHOLD 0.034
#define INV_THRESHOLD_NEG 0.004
#define ACK_THRESHOLD ?
#define BYE_THRESHOLD ?

// The average value of all distances for different types of messages
#define AVG_HELLINGER_INVITE 0.0034
#define AVG_HELLINGER_ACK 0.01
#define AVG_HELLINGER_BYE 0.01

#define ROWS 6
#define COLS 3

// File constants
const char *inputFile;
const char *outputFile;
const char *outputPath;
const char *attackFilePath;

enum FILETYPE{TRAIN_FILE, TEST_FILE};
enum MESSAGES{INVITE, ACK, BYE};

#endif // CONSTANTS_H_INCLUDED
