/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <QtSQL>
#include <QFileDialog>
#include <QMessageBox>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlQuery>
#include <QString>
#include <QTextStream>
#include "ui_mainwindow.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <stdio.h>
#include "scypeThread.h"

extern "C" {
#include "SCYPE_5179/constants.h"
#include "SCYPE_5179/readWrite.h"
#include "SCYPE_5179/mergeOperations.h"
#include "SCYPE_5179/hellingerFunctions.h"
#include "SCYPE_5179/computeFalseAlarms.h"
}

using namespace std;

scypeThread::scypeThread(int choice, QString tableName)
{
    cout << "Constructor of scypeThread just called" << endl;
    this->choice = choice;
    this->tableName = tableName;
}

scypeThread::scypeThread(int choice)
{
    cout << "Constructor of scypeThread just called" << endl;
    this->choice = choice;
}

void scypeThread::run()
{

    if(this->choice == FALSE_ALARMS)
    {
        computeEntropyFalseAlarms();
        emit entropyTestSignal("ENTROPY_FALSE_ALARM_CALCULATION");
    }
    else if(this->choice == HELLINGER_DISTANCE_)
    {
        readLogFile(ENTROPY_);
        BLOCK1 = rounds;
        BLOCK2 = rounds;
      
        mainHellingerFunction(HELLINGER_DISTANCE_);
    }
    else if(this->choice == HELLINGER_DISTANCE_1)
    {
        readLogFile(ENTROPY_);
        BLOCK2 = rounds;
        hellingerInvDetectionThreshold = trueHellingerAvgInv + (sqrt(varianceInv/numOfInvites));
        mainHellingerFunction(HELLINGER_DISTANCE_1); 
        emit hellingerDistanceTestSignal("HELLINGER_TRAIN_TEST_FINISHED");
    }
    else if(this->choice == MACHINE_LEARNING_)
    {
        readLogFile(MACHINE_LEARNING_);
        mergeFilesInDirectory();
    }
    else if(this->choice == ENTROPY_)
    {
        readLogFile(ENTROPY_);

        QSqlDatabase db;
        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("C:\\sqlite\\scype_5179.db");
        db.open();

        QSqlQuery query(db);
        QString buildQuery;
        buildQuery.append("CREATE TABLE ");
        buildQuery.append(tableName);
        buildQuery.append("(Message int, Inf1 float, Inf2 float, Inf3 float, Inf4 float, Inf5 float, Inf6 float, Inf7 float,"
             "ActualInfo float, NormalAverage float, TheoMax float, MaxNormalAverage float,"
             "ActInfoNormAver float, Distance float,  MEANVALUE float, STDEVIATION float, PROBABILITY float, ENTROPY float); ");
        cout << "TO QUERY THA EINAI.. :" << buildQuery.toStdString() << endl;
        query.exec(buildQuery);

        FILE *fp;
        char line[100];
        string composedQuery;
        string insertQuery;
        fp = fopen(outputFile, "r");

        while(fgets(line, 100, fp) != NULL)
        {
               string myline(line);
               if(myline.length())
               {

                   vector<string> sep = split(myline, ',');

                   for(int i = 0; i < sep.size(); ++i)
                   {
                        if(sep[i].find("<-->") != string::npos)
                        {
                           sep[i] = "0";
                        }
                        composedQuery += sep[i];
                        if(i<(sep.size()-1))
                        composedQuery +=",";
                   }
                   insertQuery = "INSERT INTO "+ tableName.toStdString() +" (Message , Inf1 , Inf2 , Inf3 , Inf4 , Inf5 , Inf6 , Inf7 ,"
             "ActualInfo ) VALUES("+ composedQuery +");";
                   cout << "Eisagw... " << insertQuery << endl;
                   query.exec(QString::fromStdString(insertQuery));
                   composedQuery.clear();
                   insertQuery.clear();
               }
        }

        string updateQuery;
        char temp[30];

        float TheoMax;
        TheoMax = (-6)*(log(1/(float)MESSAGE_WINDOW)/log(2));
        sprintf(temp, "%.4g", TheoMax );

        string theoMaxString(temp);

        // SET THEOMAX
        updateQuery = "UPDATE "+tableName.toStdString()+" SET TheoMax="+theoMaxString;
        query.exec(QString::fromStdString(updateQuery));
        // SET MEANVALUE
        updateQuery = "UPDATE "+tableName.toStdString()+" SET MEANVALUE=(SELECT Avg(ActualInfo) FROM SCYPE_TRAIN)";
        query.exec(QString::fromStdString(updateQuery));


        ////////////////////////////////////////////////////////////////
        // Calculations for STDEVIATION...
        float variance=0, stDeviation=0, meanValue=0;
        int records=0;

        updateQuery = "SELECT ActualInfo FROM "+ tableName.toStdString();
        query.exec(QString::fromStdString(updateQuery));

        while (query.next()) {
                records++;
                meanValue += query.value(0).toFloat();
            }

        meanValue = meanValue/records;
        cout << "To mean cvalue tha einai " << meanValue << endl;
        query.seek(0, false);
        while (query.next()) {
                variance += pow((query.value(0).toFloat() - meanValue), 2);
                cout << "To VARIANCE tha einai " << variance << endl;
            }
        variance = (float)variance/records;
        cout << "To PRAGMATIKO VARIANCE tha einai " << variance << endl;
        stDeviation = sqrt(variance);
        //////////////////////////////////////////////////////////////////////////

        // UPDATING STDEVIATION IN TABLE...
        sprintf(temp, "%.4g", stDeviation);
        string stDeviationString(temp);
        updateQuery = "UPDATE "+tableName.toStdString()+" SET STDEVIATION="+stDeviationString;
        query.exec(QString::fromStdString(updateQuery));

        float probability=1/(float)MESSAGE_WINDOW;
        sprintf(temp, "%.4g", probability);
        string probabilityString(temp);
        updateQuery = "UPDATE "+tableName.toStdString()+" SET PROBABILITY="+probabilityString;
        query.exec(QString::fromStdString(updateQuery));

        float entropy=-log(1/MESSAGE_WINDOW)/log(2);
        sprintf(temp, "%.4g", entropy);
        string entropyString(temp);
        updateQuery = "UPDATE "+tableName.toStdString()+" SET ENTROPY="+entropyString;
        query.exec(QString::fromStdString(updateQuery));


        if(!QString::compare(tableName, "SCYPE_TRAIN", Qt::CaseInsensitive))
        {
            updateQuery = "UPDATE "+tableName.toStdString()+" SET NormalAverage=TheoMax-MEANVALUE+STDEVIATION";
        }
        else
        {
            updateQuery = "UPDATE "+tableName.toStdString()+" SET NormalAverage=(SELECT NormalAverage FROM SCYPE_TRAIN)";
        }


        query.exec(QString::fromStdString(updateQuery));


        updateQuery = "UPDATE "+tableName.toStdString()+" SET MaxNormalAverage=TheoMax-NormalAverage";
        query.exec(QString::fromStdString(updateQuery));

        updateQuery = "UPDATE "+tableName.toStdString()+" SET ActInfoNormAver=ActualInfo-NormalAverage";
        query.exec(QString::fromStdString(updateQuery));

        updateQuery = "UPDATE "+tableName.toStdString()+" SET Distance=MaxNormalAverage-ActInfoNormAver";
        query.exec(QString::fromStdString(updateQuery));

        if(!QString::compare(tableName, "SCYPE_TRAIN", Qt::CaseInsensitive))
        {

            // Calculate detection threshold if it is train period...
            updateQuery = "SELECT Distance FROM SCYPE_TRAIN";
            query.exec(QString::fromStdString(updateQuery));

            ////////////////////////////////////////////////////////////////
            // Calculations for ENTROPY DETECTION THRESHOLD IN TRAIN PERIOD...

            meanValue=0;
            records=0;
            while (query.next()) {
                    records++;
                    meanValue += query.value(0).toFloat();
               
                }
            entropyDetectionThreshold = meanValue/records;

        }
        else
        {

            sprintf(temp, "%.4g", entropyDetectionThreshold);
            string threshold(temp);
            updateQuery = "SELECT Distance FROM SCYPE_TEST WHERE Distance>"+threshold;
            query.exec(QString::fromStdString(updateQuery));

            ////////////////////////////////////////////////////////////////
            records=0;
            strcpy(temp, outputPath);
            strcat(temp, "attackMessages.txt");

            FILE *fp=NULL;
            fp=fopen(temp, "w+");

            while (query.next()) {
                    records++;
                    fprintf(fp, "%d\n", records);
            }

            fclose(fp);

            emit entropyTestSignal("ENTROPY_TRAIN_TEST_FINISHED");
        }

        fclose(fp);
        db.close();

    }
}


// You could also take an existing vector as a parameter.
vector<string> scypeThread::split(string str, char delimiter) {

    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, delimiter)) {
      internal.push_back(tok);
    }

    return internal;
}


