/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "hellingerFunctions.h"
#include "headerFunctions.h"
#include "constants.h"

// This is the main function called when we want to analyze with hellinger distance
void mainHellingerFunction(int choice)
{
    int i=0;
    // We declare a 2-dimension array in order to save
    // the values of occurences per different types of messages (INVITE, ACK, BYE)
    float A[ROWS][COLS]= {0.0};
    char temporary[120];

    /* The first loop reads the already processed files to
     extract and compute the mean values of occurences for INVITE messages per header*/
    for(i=0; i<BLOCK1; i++)
    {
        returnConcatenatedPath(choice, i+1, temporary);
        readSipMessagesOccurences(TRAIN_FILE, temporary, A);
    }
    // We should normalize the mean values of occurences
    normalize2DTable(A);

    /* The second loop reads the already processed files to
     extract the occurences and compute the hellinger distance value from the previous mentioned average values*/
    for(i=0; i<BLOCK2; i++)
    {
        returnConcatenatedPath(HELLINGER_DISTANCE_1, i+1, temporary);
        readSipMessagesOccurences(TEST_FILE, temporary, A);
    }
}

// This function extracts the
void readSipMessagesOccurences(int typeOfFile, char *file, float normalMessage[][COLS])
{
    // -------------------------------------------------------------
    FILE *openSipFile=NULL;
    char *tmp=NULL, *tmp1=NULL;
    char line[160]; float *attackMessage;
    int occurences=0, x=0, symbolCounter=0, currentSymbol=0, i=0, j=0, currentSymbolAndOccurences[2];
    int flagInvite=0, flagAck=0, flagBye=0, attackFlag=0, attackHeader=0;

    // We need static variables because we want to store states from previous calls
    static int numOfInvites=0, numOfAcks=0, numOfByes=0, rounds=0;
    static int hellingerAttackMessagesInv=0, TPInv=0, FPInv=0, FNInv=0, attackMessagesInv=0, totalMessagesInv=0;
    static int hellingerAttackMessagesAck=0, TPAck=0, FPAck=0, FNAck=0, attackMessagesAck=0, totalMessagesAck=0;
    static int hellingerAttackMessagesBye=0, TPBye=0, FPBye=0, FNBye=0, attackMessagesBye=0, totalMessagesBye=0;
    static float hellingerInv=0.0, varianceInv=0.0, stDeviationInv=0.0, hellingerAvgInv=0.0;
    static float hellingerAck=0.0, varianceAck=0.0, stDeviationAck=0.0, hellingerAvgAck=0.0;
    static float hellingerBye=0.0, varianceBye=0.0, stDeviationBye=0.0, hellingerAvgBye=0.0;

    attackMessage = (float *) malloc(ROWS*sizeof(float));
    // Trafic file
    openSipFile = fopen(file, "r");
    //printf("\nDiavazw apo to path.. %s",file);
    // ------------------------------------
    while(fgets(line, 160, openSipFile)!=NULL)
    {
        // If the current line contains a valid symbol proceed
        if(currentSymbol = headersToSipMessage(line))
        {
                   // If the current line corresponds to an INVITE first line header
                   if(strstr(line,"INVITE"))
                   {
                        // Activate an appropriate flag
                        flagInvite=1;
                        // Count the number of invites
                        if(type)
                           numOfInvites++;
                   }
                   else if(strstr(line,"ACK"))
                   {
                        flagAck=1;
                        // Count the number of invites
                        numOfAcks++;
                   }
                   else if(strstr(line, "BYE"))
                   {
                        flagBye=1;
                        // Count the number of invites
                        numOfByes++;
                   }
                   if(currentSymbol==333)
                   {
                        if(typeOfFile==TRAIN_FILE)
                        {
                            normalMessage[5][1]=0;
                            normalMessage[5][2]=0;
                        }
                        else
                        {
                            attackMessage[5]=0;
                        }
                   }
                   // If it is an attack message increment the appropriate counter
                   attackHeader = isAttackMessage(line);
                            if(attackHeader && flagInvite)
                            {
                                attackMessagesInv++;
                                attackFlag=1;
                            }
                            else if(attackHeader && flagAck)
                            {
                                attackMessagesAck++;
                                attackFlag=1;
                            }
                            else if(attackHeader && flagBye)
                            {
                                attackMessagesBye++;
                                attackFlag=1;
                            }
             }
             else if(strstr(line,"$$"))
             {
                   // This part of the code extracts the number of occurences
                   extractNumberFromString(line, currentSymbolAndOccurences);

                   if(flagInvite)
                   {
                        if(typeOfFile==TRAIN_FILE)
                        {
                           normalMessage[currentSymbolAndOccurences[0]-1][0] +=((float)currentSymbolAndOccurences[1]);
                        }
                        else if(typeOfFile==TEST_FILE)
                        {
                           attackMessage[currentSymbolAndOccurences[0]-1] +=((float)currentSymbolAndOccurences[1]);
                        }
                   }
                   // Code must be written for the other two cases {ACK, BYE}
             }
             else if(strstr(line, "--"))
             {
                   if(typeOfFile==TEST_FILE)
                   {
                         normalizeTable(attackMessage);
                         if(flagInvite==1)
                         {
                             // Calculates the hellinger distance
                             hellingerInv = hellingerDistance(INVITE, attackMessage, normalMessage);
                             // Calculates the variance for the current hellinger distance value
                             varianceInv+= pow((hellingerInv-AVG_HELLINGER_INVITE), 2);

                             // DETECTION...
                             if( (hellingerInv>INV_THRESHOLD) )
                             {
                                 //printf("\nTo minima me noumero : %d einai hellinger minima epithesis", totalMessagesInv);
                                 hellingerAttackMessagesInv++;
                                 if(attackFlag==1)
                                 {
                                     TPInv++;
                                 }
                                 else{
                                     FPInv++;
                                 }
                             }
                             else
                             {
                                if(attackFlag==1)
                                {
                                    FNInv++;
                                }
                             }
                             hellingerAvgInv+=hellingerInv;
                             totalMessagesInv++;
                         }
                    }
                    flagInvite=0, flagAck=0, flagBye=0, attackFlag=0;
             }
         }
         fclose(openSipFile);
         free(attackMessage);

         if(++rounds == BLOCK1)
         {
             computeMeanValues2DTable(normalMessage, numOfInvites);
         }

         printf("\nTotal INVITE requests from test files : %d", numOfInvites);
         printf("\nTotal ACK requests from test files : %d", numOfAcks);
         printf("\nTotal BYE requests from test files : %d", numOfByes);
         printf("\nThe total number of messages from TEST files will be : %d", numOfInvites+numOfAcks+numOfByes);

         printf("\n\nTotal INVITE requests recognized by hellinger : [%d]", hellingerAttackMessagesInv);
         printf("\nThe TP messages of attack : [%d], .. [%d]", attackMessagesInv, TPInv);
         printf("\nThe total number of INVITE requests included in test files : [%d]", totalMessagesInv);
         printf("\nThe total number of FNs : %d", FNInv);
         printf("\nThe total number of FPs : %d", FPInv);
         printf("\nThe SUM of all distances : [%f], The total number of messages in test files [%d]", hellingerAvgInv, totalMessagesInv);
         printf("\nThe average value of hellinger distance : [%f]", hellingerAvgInv/totalMessagesInv);
         printf("\nThe variance : [%f]", varianceInv/totalMessagesInv);
         printf("\nThe standard deviation : [%f]", sqrt(varianceInv/totalMessagesInv));

}

// This function divides every element of a 2D array with the number of messages
void computeMeanValues2DTable(float K[][COLS], int normalInvites)
{
    int i,j;
    for(i=0; i<6; i++)
        for(j=0; j<3; j++)
        {
             if(j==0)
                K[i][j]=ceil(K[i][j]/normalInvites);
             //else if(j==1)
                //K[i][j]=ceil(K[i][j]/normalAcks);
             //else if(j==2)
                //K[i][j]=ceil(K[i][j]/normalByes);
        }
}

// This function normalizes a 2D-array
void normalize2DTable(float K[][COLS])
{
    int i=0, j=0;
    float SUM1=0, SUM2=0, SUM3=0;

    for(i=0; i<6; i++)
    {
        for(j=0; j<3; j++)
        {
           if(j==0)
             SUM1+= K[i][j];
           if(j==1)
             SUM2+= K[i][j];
           if(j==2)
             SUM3+= K[i][j];
        }
    }

    for(i=0; i<6; i++)
    {
        for(j=0; j<3; j++)
        {
            if(j==0)
            {
                K[i][j]= K[i][j]/SUM1;
            }
            else if(j==1)
            {
                K[i][j]= K[i][j]/SUM2;
            }
            else if(j==2)
            {
                K[i][j]= K[i][j]/SUM3;
            }

        }
    }
}

// This function prints a 1D array
void printTable(float K[])
{
     int i;
     for(i=0; i<6; i++)
     {
             printf("\n1D-Element %f", K[i]);
     }
}

// This function prints a 2D array
void print2DTable(float K[][COLS])
{
     int i, j;
     for(i=0; i<6; i++)
     {
        for(j=0; j<3; j++)
        {
             printf("\n2D-Element %f", K[i][j]);
        }
     }
}

// This function calculates the hellinger distance between two messages
float hellingerDistance(int messageType, float A[], float B[][COLS])
{
    int i=0, j=0;
    float hellinger=0.0;

    for(i=0; i<6; i++)
    {
            if(messageType==INVITE)
            {
                 hellinger+= pow(sqrt(A[i]) - sqrt(B[i][0]), 2);
                 //continue;
            }/*
            else if(messageType==ACK)
               hellinger+= pow(sqrt(A[i]) - sqrt(B[i][1]), 2);
            else if(messageType==BYE)
               hellinger+= pow(sqrt(A[i]) - sqrt(B[i][2]), 2);*/
    }
    return hellinger*0.5;
}

// This function normalizes a 1D array
void normalizeTable(float *K)
{
    int i=0;
    float SUM=0.0;

    for(i=0; i<6; i++)
    {
        SUM+= K[i];
    }

    for(i=0; i<6; i++)
    {
        K[i]=K[i]/SUM;
    }
}
