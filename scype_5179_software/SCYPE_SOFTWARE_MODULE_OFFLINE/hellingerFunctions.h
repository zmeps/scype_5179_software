/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#ifndef HELLINGERFUNCTIONS_H_INCLUDED
#define HELLINGERFUNCTIONS_H_INCLUDED

#include "constants.h"

void mainHellingerFunction(int);
void readSipMessagesOccurences(int, char *, float [][COLS]);
void checkAttackFile(char *,  float [][COLS]);
float hellingerDistance(int, float [], float [][COLS]);
void normalizeTable(float *);
void normalize2DTable(float [][COLS]);
void print2DTable(float [][COLS]);
void computeMeanValues2DTable(float [][COLS], int);


#endif // HELLINGERFUNCTIONS_H_INCLUDED
