/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#include <iostream>
#include <ctime>
#include <string.h>
#include <sys/stat.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "scypeThread.h"

extern "C" {
#include "SCYPE_5179/constants.h"
#include "SCYPE_5179/readWrite.h"
#include "SCYPE_5179/mergeOperations.h"
}

#include <QFileDialog>
#include <QMessageBox>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(540,650);
    ui->pushButton_2->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
    ui->pushButton_7->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete myScypeThread;
}

void MainWindow::on_pushButton_clicked()
{

   MESSAGE_WINDOW = (ui->lineEdit->text()).toInt();

   switch(ui->comboBox->currentIndex())
   {

       case 0:
       QMessageBox::information(this, tr("NOT VALID"), "Please select a valid option"
                               "\n");
       ui->textEdit->append("\nWrong Option...");
       break;

       case 1:

           ui->textEdit->append("Train Entropy...");
           if(strlen(sipTrainFile))
           {
              ui->textEdit->append("Analyzing Train file with Entropy!!");
              inputFile = sipTrainFile;
              outputPath = outputTrainPath;
              outputFile = sipTrainEntropyMetricsFile;
              myScypeThread = new scypeThread(ENTROPY_, "SCYPE_TRAIN");
              myScypeThread->start();

           }
           else
           {
               ui->textEdit->append("Be Careful No Train file was provided!!");
           }
       break;

       case 2:
           if(strlen(sipTestFile))
           {

               ui->textEdit->append("Analyzing Test file with Entropy!!");
               rounds=0;
               inputFile = sipTestFile;
               outputPath = outputTestPath;
               outputFile = sipTestEntropyMetricsFile;

               myScypeThread = new scypeThread(ENTROPY_, "SCYPE_TEST");

                   // Connect our signal and slot
                   connect(myScypeThread, SIGNAL(entropyTestSignal(QString)),
                                         SLOT(onEntropyTestSignal(QString)));
                   // Setup callback for cleanup when it finishes
                   connect(myScypeThread, SIGNAL(finished()),
                           myScypeThread, SLOT(deleteLater()));

               myScypeThread->start();

           }
           else
           {
               ui->textEdit->append("Be Careful No Test file was provided!!");
           }

       break;

       case 3:

           if(strlen(sipTrainFile))
           {

               ui->textEdit->append("Analyzing Train file with Hellinger Distance!!");
               inputFile = sipTrainFile;
               outputPath = outputTrainPath;
               outputFile = sipTrainEntropyMetricsFile;
               myScypeThread = new scypeThread(HELLINGER_DISTANCE_);
               myScypeThread->start();
           }
           else
           {
               ui->textEdit->append("Be Careful No Train file was provided!!");
           }

       break;


       case 4:

           if(strlen(sipTrainFile))
           {
               ui->textEdit->append("Analyzing Test file with Hellinger Distance!!");
               rounds=0;
               inputFile = sipTestFile;
               outputPath = outputTestPath;
               outputFile = sipTestEntropyMetricsFile;
               myScypeThread = new scypeThread(HELLINGER_DISTANCE_1);

                   // Connect our signal and slot
                   connect(myScypeThread, SIGNAL(hellingerDistanceTestSignal(QString)),
                                         SLOT(onHellingerDistanceTestSignal(QString)));
                   // Setup callback for cleanup when it finishes
                   connect(myScypeThread, SIGNAL(finished()),
                           myScypeThread, SLOT(deleteLater()));

               myScypeThread->start();
           }
           else
           {
               ui->textEdit->append("Be Careful No Test file was provided!!");

           }

       break;

       case 5:

           if(strlen(sipTrainFile))
           {
               ui->textEdit->append("Analyzing Train file with ML!!");
               inputFile = sipTrainFile;
               outputPath = outputTrainPath;
               myScypeThread = new scypeThread(MACHINE_LEARNING_);
               myScypeThread->start();
           }
           else
           {
               ui->textEdit->append("Be Careful No Train file was provided!!");
           }

       break;

       case 6:
           if(strlen(sipTestFile))
           {

               ui->textEdit->append("Analyzing T file with ML!!");
               rounds=0;
               inputFile = sipTestFile;
               outputPath = outputTestPath;
               myScypeThread = new scypeThread(MACHINE_LEARNING_);
               myScypeThread->start();

           }
           else
           {
               ui->textEdit->append("Be Careful No Test file was provided!!");
           }
       break;
   }
}

void MainWindow::on_pushButton_2_clicked()
{
    QString trainFile = QFileDialog::getOpenFileName(this, tr("File Explorer"), "C://", "All files (*.*)");
    if(trainFile.length())
    {
        string input = trainFile.toStdString();
        sipTrainFile = new char [input.size()+2];
        strcpy( sipTrainFile, input.c_str() );
        QMessageBox::information(this, tr("Selected File"), sipTrainFile);
    }


}

void MainWindow::on_pushButton_4_clicked()
{
    QString testFile = QFileDialog::getOpenFileName(this, tr("File Explorer"), "C://", "All files (*.*)");
    if(testFile.length())
    {
        string input = testFile.toStdString();
        sipTestFile = new char [input.size()+2];
        strcpy( sipTestFile, input.c_str() );
        QMessageBox::information(this, tr("Selected File"), sipTestFile);
    }

}

void MainWindow::on_pushButton_3_clicked()
{
    char temp[100];
    QString currentPath = QFileDialog::getExistingDirectory(this, tr("Path Explorer"), "C://");
    if(currentPath.length())
    {
        // Chooose OUTPUT path...
        string fname = currentPath.toStdString();
        outputPath = new char [fname.size()+2];
        strcpy(outputPath, fname.c_str());
        outputPath[fname.size()] = '/';
        outputPath[fname.size()+1] = '\0';
        QMessageBox::information(this, tr("Selected Output Path"), outputPath);

        strcpy(temp, outputPath);
        strcat(temp, "trainDirectory/");
        mkdir(temp);
        QMessageBox::information(this, tr("Selected Output Path"), temp);

        outputTrainPath = new char [strlen(temp)+2];
        strcpy(outputTrainPath, temp);

        strcat(temp, "sipTrainResultFile.txt");
        sipTrainEntropyMetricsFile = new char[strlen(temp)+1];
        strcpy(sipTrainEntropyMetricsFile, temp);
        QMessageBox::information(this, tr("Selected Output Path"), sipTrainEntropyMetricsFile);

        /////////////////////////////////////////

        strcpy(temp, outputPath);
        strcat(temp, "testDirectory/");
        mkdir(temp);
        QMessageBox::information(this, tr("Selected Output Path"), temp);

        outputTestPath = new char [strlen(temp) +2];
        strcpy(outputTestPath, temp);

        strcat(temp, "sipTestResultFile.txt");
        sipTestEntropyMetricsFile = new char[strlen(temp)+1];
        strcpy(sipTestEntropyMetricsFile, temp);
        QMessageBox::information(this, tr("Selected Output Path"), sipTestEntropyMetricsFile);
    }

}

void MainWindow::on_pushButton_5_clicked()
{
        ui->comboBox->setCurrentIndex(-1);
        ui->lineEdit->clear();
        ui->textEdit->clear();
        ui->pushButton_2->setEnabled(false);
        ui->pushButton_3->setEnabled(false);
        ui->pushButton_4->setEnabled(false);
        ui->pushButton_7->setEnabled(false);
}

void MainWindow::on_actionAbout_triggered()
{
        QMessageBox::information(this, tr("ABOUT THIS SOFTWARE"),

                            "\nAuthor : Zisis Tsiatsikas"
                            "\nEmail  : tzisis@aegean.gr"
                            "\n"
                            "\nDescription :"
                            "\n"
                            "\nThis module is part of the 5179 (SCYPE) research project, "
                            "implemented within the context of the Greek Ministry of "
                            "Development-General Secretariat of Research and Technology "
                            "funded program “Excellence II / Aristeia II”, co-financed "
                            "by the European Union/European Social Fund - Operational "
                            "program “Education and Life-long Learning” and National funds. "
                            "\n"
                            "\n"
                            "\nDate : 31/10/2015"
                            "\nVersion : 2.0.0"
                            "\n"
                            "\nscype.samos.aegean.gr"
                            "\n");
}

void MainWindow::on_pushButton_6_clicked()
{
       exit(0);
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{

       if(index==0)
       {
            ui->pushButton_2->setEnabled(false);
            ui->pushButton_3->setEnabled(false);
            ui->pushButton_4->setEnabled(false);
       }
       else if(index==1)
       {
            ui->pushButton_2->setEnabled(true);
            ui->pushButton_3->setEnabled(true);
            ui->pushButton_4->setEnabled(false);
       }
       else if(index==2)
       {
            ui->pushButton_2->setEnabled(false);
            ui->pushButton_3->setEnabled(true);
            ui->pushButton_4->setEnabled(true);
       }
       else if(index==3)
       {
            ui->pushButton_2->setEnabled(true);
            ui->pushButton_3->setEnabled(true);
            ui->pushButton_4->setEnabled(false);
       }
       else if(index==4)
       {
            ui->pushButton_2->setEnabled(false);
            ui->pushButton_3->setEnabled(true);
            ui->pushButton_4->setEnabled(true);
       }
       else if(index==5)
       {
           ui->pushButton_2->setEnabled(true);
           ui->pushButton_3->setEnabled(true);
           ui->pushButton_4->setEnabled(false);
       }
       else if(index==6)
       {
           ui->pushButton_2->setEnabled(false);
           ui->pushButton_3->setEnabled(true);
           ui->pushButton_4->setEnabled(true);
       }
}

void MainWindow::on_pushButton_7_clicked()
{
       myScypeThread = new scypeThread(FALSE_ALARMS);

           // Connect our signal and slot
           connect(myScypeThread, SIGNAL(entropyTestSignal(QString)),
                                 SLOT(onEntropyTestSignal(QString)));
           // Setup callback for cleanup when it finishes
           connect(myScypeThread, SIGNAL(finished()),
                   myScypeThread, SLOT(deleteLater()));

       myScypeThread->start();
}

void MainWindow::onEntropyTestSignal(QString info)
{
       char temp[10];

       if(!QString::compare(info, "ENTROPY_TRAIN_TEST_FINISHED", Qt::CaseInsensitive))
       {
           ui->pushButton_7->setEnabled(true);
           ui->textEdit->append("Entropy train and test analysis has finished. You can now calculate false alarms!!");
       }
       else if(!QString::compare(info, "ENTROPY_FALSE_ALARM_CALCULATION", Qt::CaseInsensitive))
       {
           ui->textEdit->append("\n----------------------------\nEntropy Results\n----------------------------");
           sprintf(temp, "%d", entropyFP);
           string FPInv(temp);
           ui->textEdit->append("FP :"+QString::fromStdString(FPInv));
           sprintf(temp, "%d", entropyFN);
           string FNInv(temp);
           ui->textEdit->append("FN :"+QString::fromStdString(FNInv));
           sprintf(temp, "%d", entropyTP);
           string TPInv(temp);
           ui->textEdit->append("TP :"+QString::fromStdString(TPInv));
           ui->textEdit->append("\n----------------------------\nEntropy False Alarm Calculation just finished!!");

       }
}

void MainWindow::onHellingerDistanceTestSignal(QString info)
{
       char temp[10];
       int totalnumMessages =numOfInvites+numOfAcks+numOfByes;

       ui->textEdit->append("\n----------------------------\nHellinger Distance Results\n----------------------------");
       sprintf(temp, "%d", FPInv);
       string FPInv(temp);
       ui->textEdit->append("FP :"+QString::fromStdString(FPInv));
       sprintf(temp, "%d", FNInv);
       string FNInv(temp);
       ui->textEdit->append("FN :"+QString::fromStdString(FNInv));
       sprintf(temp, "%d", TPInv);
       string TPInv(temp);
       ui->textEdit->append("TP :"+QString::fromStdString(TPInv));
       sprintf(temp, "%d", numOfInvites);
       string numOfInvites(temp);
       ui->textEdit->append("Total Number of Invite Messages :"+QString::fromStdString(numOfInvites));
       sprintf(temp, "%d", (totalnumMessages));
       string totalMessages(temp);
       ui->textEdit->append("Total Number of Messages :"+QString::fromStdString(totalMessages));


       ui->textEdit->append("\n----------------------------\nHellinger Distance analysis has finished!!");

}
