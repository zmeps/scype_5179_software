/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#include <stdio.h>

#include "headerFunctions.h"
#include "constants.h"

// This function returns the current symbol.
// It uses a static counter to count if the next headers are valid.
// ACK and BYE requests have one less header - symbol
int findHeader(char line[160], int *currentSymbol)
{
    /*
      - INVITE: Let invite a user or a service to a new session or to modify parameters of a established session.
      - ACK: Confirm the session establishment
      - OPTION: Request information about the capabilities of a server
      - BYE: End of a session
      - CANCEL: Cancel a pending request.
      - REGISTER: Register the user agent.
    */

    static int flagFrstLine=1;
    static int scndVia=0;
    static int sumofHeaders=0;
    char *s=NULL;

        s = strstr(line, "INVITE sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 4;
            return sumofHeaders;
        }
        s = strstr(line, "ACK sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=5;
            //return 4;
            return sumofHeaders;
        }
        s = strstr(line, "BYE sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=5;
            //return 4;
            return sumofHeaders;
        }
        s = strstr(line, "CANCEL sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 5;
            return sumofHeaders;
        }
        s = strstr(line, "OPTIONS sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 5;
            return sumofHeaders;
        }
        s = strstr(line, "REGISTER sip:");
        if( s != NULL)
        {
            *currentSymbol = 1;
            flagFrstLine = 0;
            scndVia=0;
            sumofHeaders=6;
            //return 5;
            return sumofHeaders;
        }

        s = strstr(line, "Via:");

        if( s!= NULL)
        {
            // Compute only the first Via message...
            if((++scndVia) == 1)
            {
                *currentSymbol = 2;
                sumofHeaders--;
                //return 1;
                return sumofHeaders;
            }
            s = NULL;
        }
        s = strstr(line, "From:");
        if( s!= NULL)
        {
            *currentSymbol = 3;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
        s = strstr(line, "To:");
        if( s!= NULL)
        {
            *currentSymbol = 4;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
        s = strstr(line, "Call-ID:");
        if( s!= NULL)
        {
            *currentSymbol = 5;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
        s = strstr(line, "Contact:");
        if( s!= NULL)
        {
            *currentSymbol = 6;
            //flagFrstLine = 1;
            sumofHeaders--;
            //return 1;
            return sumofHeaders;
        }
    return 0;
}

// This function takes as an input a line from the log file and
// returns the current symbol of the message.
// If the line does not contain a valid symbol then this function returns zero.
int headersToSipMessage(char *line)
{
    int currentSymbol, isAckBye=0, numOfMessages=0, sumHeaders=0;
    static int symbolCounter=0;
    if(findHeader(line, &currentSymbol) > 0)
    {
          if((++symbolCounter==6 || (isAckBye = isAckByeMessage(line, symbolCounter))))
          {
                symbolCounter=0; // Set the initial value to the appropriate counters
                if(isAckBye)
                {
                    return 333;
                }
                else
                {
                    return 666;
                }
          }
          return currentSymbol;
    }
    return 0;
}

// This function returns 1 if the current line indicates
// the beggining of an ACK or BYE message
int isAckByeMessage(char *line, int symbolCounter)
{
   static int valid=0;

   if(strstr(line, "ACK sip:")!=NULL)
   {
       valid = 1;
   }
   else if(strstr(line, "BYE sip:")!=NULL)
   {
       valid = 1;
   }

   if(valid==1 && symbolCounter==5)
   {
       valid = 0;
       return 1;
   }
   else
       return 0;
}

// This function returns 1 if the examined message is a real attack message
// Real attack messages are generated by the tools with specific ports
int isAttackMessage(char *line)
{
    if(strstr(line, ":333") || strstr(line, ":3"))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

// This function returns a concatenated path
// We use this function with the aim to examine the generated files
void returnConcatenatedPath(int choice, int rounds, char *temporary)
{
    char temp[60];
    FILE * resultFile=NULL;

    sprintf(temporary, outputPath);
    if(choice==ENTROPY_ || choice==HELLINGER_DISTANCE_)
    {
        sprintf(temp,"%d",rounds);
        strcat(temporary,temp);
        strcat(temporary,".txt");
        printf("\n to tempora tha einai : %s, ston guro : %d", temporary, rounds);
    }
    else if(choice==HELLINGER_DISTANCE_1)
    {
        sprintf(temporary, attackFilePath);

        sprintf(temp,"%d",rounds);
        strcat(temporary,temp);
        strcat(temporary,".txt");
        printf("\n to tempora tha einai : %s, ston guro : %d", temporary, rounds);
    }
    else if(choice==MACHINE_LEARNING_)
    {
        sprintf(temp,"%d",rounds);
        strcat(temporary,temp);
        strcat(temporary,".arff");

        if(rounds==1)
        {
           resultFile = fopen(temporary,"w+");
           fprintf(resultFile, "@RELATION test\n\n@ATTRIBUTE s1 NUMERIC\n@ATTRIBUTE s2 NUMERIC\n@ATTRIBUTE s3 NUMERIC\n");
           fprintf(resultFile, "@ATTRIBUTE s4 NUMERIC\n@ATTRIBUTE s5 NUMERIC\n@ATTRIBUTE s6 NUMERIC\n@ATTRIBUTE class {attack, normal}\n\n");
           fprintf(resultFile, "@DATA\n");
           fclose(resultFile);
        }
    }
}

// This function extracts the current symbol and the number of occurences
// It returns these values in the form of a table
void extractNumberFromString(char *line, int *currentSymbolAndOccurences)
{
       int number;
       char *tmp=NULL;
       tmp = strtok (line, "Hs");
       tmp = strtok(NULL, "Hs");
       sscanf(tmp, "%d", &number);
       currentSymbolAndOccurences[0]=number;
       tmp = strtok (tmp, "Ap##");
       tmp = strtok(NULL, "Ap##");
       sscanf(tmp, "%d", &number);

       currentSymbolAndOccurences[1]=number;
}
