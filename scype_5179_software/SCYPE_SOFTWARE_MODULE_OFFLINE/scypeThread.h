/**
*    Author : Zisis Tsiatsikas
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/10/2015
*    Version : 2.0.0
*/

#ifndef THREAD_H
#define THREAD_H

#include <QThread>
#include <vector>
#include <QtSql/QSqlDatabase>

using namespace std;
using std::vector;

class scypeThread : public QThread
{
    Q_OBJECT

private:
    int choice;
    QString tableName;


public:
    scypeThread(int, QString);
    scypeThread(int);


protected:
    void run();
    vector<string> split(string, char);
    void closeConncetion(void);



// Define signal:
    signals:
    void entropyTestSignal(QString info);
    void hellingerDistanceTestSignal(QString info);
};

#endif // THREAD_H
