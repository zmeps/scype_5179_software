/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#include <stdio.h>
#include <stdlib.h>

// This function merges the files in one folder into one
void mergeFilesInDirectory(char *temporary, int numOfFiles)
{
    FILE *fs1, *fs2;
    char temp[60], k;

    char ch;
    int i=0;

    for(i=1; i<=numOfFiles; i++)
    {
           fs2 = fopen("/sipResultFile.arff","a+");
           sprintf(temp,"%d",i);
           strcat(temporary,temp);
           strcat(temporary,".arff");
           printf("\n%s", temporary);
           fs1 = fopen(temporary,"r");

           if(fs1==NULL)
           {
              perror("Error ");
              printf("Press any key to exit...\n");
              getch();
              exit(EXIT_FAILURE);
           }
           else
           {
              while((ch=fgetc(fs1))!=EOF)
              {
                  fputc(ch,fs2);
              }
           }

           printf("Two files were merged into file successfully.\n");
           fclose(fs1);
           fclose(fs2);
   }
}
