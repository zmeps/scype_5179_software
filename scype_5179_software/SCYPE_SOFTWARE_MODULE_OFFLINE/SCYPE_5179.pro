#-------------------------------------------------
#
# Project created by QtCreator 2015-05-15T06:39:09
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SCYPE_5179
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    SCYPE_5179/encryptLogFile.c \
    SCYPE_5179/entropyMetrics.c \
    SCYPE_5179/hashTableOperations.c \
    SCYPE_5179/headerFunctions.c \
    SCYPE_5179/hellingerFunctions.c \
    SCYPE_5179/mergeOperations.c \
    SCYPE_5179/readWrite.c \
    SCYPE_5179/constants.c \
    scypeThread.cpp \
    SCYPE_5179/computeFalseAlarms.c


HEADERS  += mainwindow.h \
    SCYPE_5179/constants.h \
    SCYPE_5179/encryptLogFile.h \
    SCYPE_5179/entropyMetrics.h \
    SCYPE_5179/hashTableOperations.h \
    SCYPE_5179/headerFunctions.h \
    SCYPE_5179/hellingerFunctions.h \
    SCYPE_5179/mergeOperations.h \
    SCYPE_5179/readWrite.h \
    scypeThread.h \
    SCYPE_5179/computeFalseAlarms.h



FORMS    += mainwindow.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../OpenSSL-Win32/lib/MinGW/ -leay32
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../OpenSSL-Win32/lib/MinGW/ -leay32
else:unix: LIBS += -L$$PWD/../../../../../OpenSSL-Win32/lib/MinGW/ -leay32
INCLUDEPATH += $$PWD/../../../../../OpenSSL-Win32/include
DEPENDPATH += $$PWD/../../../../../OpenSSL-Win32/include


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../GTK/lib/ -lglib-2.0.dll
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../GTK/lib/ -lglib-2.0.dll
else:unix: LIBS += -L$$PWD/../../../../../GTK/lib/ -lglib-2.0.dll

INCLUDEPATH += $$PWD/../../../../../GTK/include/glib-2.0/
DEPENDPATH += $$PWD/../../../../../GTK/include/glib-2.0/


