/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <gtk/gtk.h>

#include "constants.h"

// Prints available methods for analysis
int menu()
{
    int choice;

    printf("\n\nSelect analysis method.\n");
    printf(" 1. Entropy.\n");
    printf(" 2. Hellinger Distance.\n");
    printf(" 3. Machine Learning.\n");
    printf("-1. Exit.\n");

    scanf("%d",&choice);
    return choice;
}

int main()
{
    // Variables decleration
    int choice;

        // Iterate to choose procession method
        while((choice = menu())!=-1)
        {
            switch(choice)
            {
                case ENTROPY_:
                        readLogFile(ENTROPY_);
                        break;
                case HELLINGER_DISTANCE_:
                        //readLogFile(ENTROPY_);
                        mainHellingerFunction(HELLINGER_DISTANCE_);
                        break;
                case MACHINE_LEARNING_:
                        readLogFile(MACHINE_LEARNING_);
                        break;
                default:
                    printf("\nTHIS IS NOT A VALID OPTION!");
            }
        }
    return 0;
}
