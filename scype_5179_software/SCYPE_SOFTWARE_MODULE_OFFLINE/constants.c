/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

#include "constants.h"

// Define the appropriate directories for input and output files
extern const char *inputFile="trainFilePath/file.txt";
extern const char *outputFile="outputPath/";
extern const char *outputPath="outputPath/";
extern const char *attackFilePath="testFilePath/";
