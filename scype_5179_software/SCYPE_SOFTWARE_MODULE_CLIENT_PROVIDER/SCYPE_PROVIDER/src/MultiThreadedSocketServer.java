/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
 
 
public class MultiThreadedSocketServer {
 
	SSLServerSocket serversocket;
    boolean ServerOn = true;
 
    public MultiThreadedSocketServer()
    {
        try
        {
        	
        	System.setProperty("javax.net.ssl.keyStore", "/SCYPE.jks"); 
        	System.setProperty("javax.net.ssl.keyStorePassword", "scype5179");
        	 
        	ServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        	serversocket = (SSLServerSocket) ssf.createServerSocket(11111);
            
            InetAddress address = InetAddress.getLocalHost();
            System.out.println(address);
        }
        catch(IOException ioe)
        {
            System.out.println("Could not create server socket on port 11111. Quitting.");
            System.exit(-1);
        }
 
        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        System.out.println("It is now : " + formatter.format(now.getTime()));
 
        // Successfully created Server Socket. Now wait for connections.
        while(ServerOn)
        {                       
            try
            {
            	SSLSocket clientSocket = (SSLSocket) serversocket.accept();
                ClientServiceThread cliThread = new ClientServiceThread(clientSocket);
                cliThread.start();

            }
            catch(IOException ioe)
            {
                System.out.println("Exception encountered on accept. Ignoring. Stack Trace :");
                ioe.printStackTrace();
            }
        }
        try
        {
        	serversocket.close();
        	//serversocket.close();
            System.out.println("Server Stopped");
        }
        catch(Exception ioe)
        {
            System.out.println("Problem stopping server socket");
            System.exit(-1);
        }
    }
 
    public static void main (String[] args)
    {
        new MultiThreadedSocketServer();       
    }
 
    class ClientServiceThread extends Thread
    {
        private Socket myClientSocket;
        private boolean m_bRunThread = true;
        private ObjectOutputStream out = null;
        private ObjectInputStream in = null;
        private String fileName;
        private final int BUFFER_SIZE = 100;  
        private FileOutputStream fos = null;  
        private byte [] buffer; 
        private Object o;  
        
        public ClientServiceThread()
        {
            super();
        }
 
        ClientServiceThread(Socket s)
        {
            myClientSocket = s;
            buffer = new byte[BUFFER_SIZE]; 
        }
 
        @SuppressWarnings("resource")
		public void run()
        {           
            
			try {
				out = new ObjectOutputStream(myClientSocket.getOutputStream());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
            
			try {
				in = new ObjectInputStream(myClientSocket.getInputStream());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
            // Print out details of this connection
            System.out.println("Accepted Client Address - " + myClientSocket.getInetAddress().getHostName());
 
            try
            {                               
             
                while(m_bRunThread)
                {                   
                    // read incoming stream
                    String clientCommand = (String) in.readObject();
                    System.out.println("Client Says :" + clientCommand);
                    
                    
                    if(!ServerOn)
                    {
                        // Special command. Quit this thread
                        System.out.print("Server has already stopped");
                        out.writeObject("Server has already stopped");
                        out.flush();
                        m_bRunThread = false;  
 
                    }
 
                    if(clientCommand.equalsIgnoreCase("quit")) {
                        // Special command. Quit this thread
                        m_bRunThread = false;  
                        System.out.print("Stopping client thread for client : ");
                        
                    } else if(clientCommand.equalsIgnoreCase("end")) {
                        // Special command. Quit this thread and Stop the Server
                        m_bRunThread = false;  
                        System.out.print("Stopping client thread for client : ");
                        ServerOn = false;
                        
                    } else if(clientCommand.equalsIgnoreCase("SEND_FILE")) {
                        
                        // 1. Read file name.                     
                        fileName = (String) in.readObject();  
                        fos = new FileOutputStream("/"+fileName);  

                        // 2. Read file to the end.  
                        Integer bytesRead = 0;  
                      
                        do {  
                            
			        o = in.readObject();    
				bytesRead = (Integer)o;  
			  
				o = in.readObject();  
				buffer = (byte[])o;  
						  
			        // 3. Write data to output file.  
			        fos.write(buffer, 0, bytesRead);  
                                
                            } while (bytesRead == BUFFER_SIZE);  
                              
                            System.out.println("File transfer success");  
                           
                    } else {
                            // Process it
                            out.println("Server Says : " + clientCommand);
                            out.flush();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                // Clean up
                try
                {                   
                    in.close();
                    out.close();
                    myClientSocket.close();
                    System.out.println("...Stopped");
                }
                catch(IOException ioe)
                {
                    ioe.printStackTrace();
                }
            }
        }
 
    }
} 
