/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JProgressBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.SwingUtilities;


public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	final JLabel statusbar;
	private SSLConnection connect;
	private JLabel lblNewLabel;
	private JLabel label_1;
    private String chosenFile=null;
    private JTextField textField_2;
    private JFileChooser chooser;
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setTitle("EntropyDetectionService/ SCYPE Client");
					frame.setResizable(false);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 438);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnInfos = new JMenu("Menu");
		menuBar.add(mnInfos);
		
		JMenu mnInfo = new JMenu("Info");
		mnInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("OK");
			}
		});
		
		mnInfos.add(mnInfo);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(15, 52, 199, 26);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblInsertRemotePort = new JLabel("Insert Remote IP/HOST_NAME");
		lblInsertRemotePort.setFont(new Font("Tahoma", Font.ITALIC, 18));
		lblInsertRemotePort.setForeground(Color.BLACK);
		lblInsertRemotePort.setBounds(15, 16, 309, 20);
		panel.add(lblInsertRemotePort);
		
		JButton btnNewButton = new JButton("SEND FILE\r\n");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				try {
					connect.send_file(chosenFile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(349, 268, 115, 42);
		panel.add(btnNewButton);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(15, 130, 146, 26);
		panel.add(textField_1);
		
		JLabel label = new JLabel("Insert Remote Port");
		label.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label.setForeground(Color.BLACK);
		label.setBounds(15, 94, 236, 20);
		panel.add(label);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(15, 172, 276, 9);
		panel.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setBounds(294, 16, 2, 294);
		panel.add(separator_1);
		
		JButton button = new JButton("CONNECT");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
		       //connect = new SSLConnection(textField.getText().toString(), Integer.parseInt(textField_1.getText().toString()));    
		       
		       lblNewLabel.setBackground(Color.GREEN);
			   label_1.setBackground(Color.RED);  
			  
					
				   SwingUtilities.invokeLater(new Runnable() {
	                    public void run() {
	                    	
	                    	try {
								
	                    		connect = new SSLConnection(textField.getText().toString(), Integer.parseInt(textField_1.getText().toString()));   
	                    		
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                    
	                    }
	                });  
			}
		});
		
		button.setBounds(30, 210, 136, 42);
		panel.add(button);
		
		JLabel lblChooseFile = new JLabel("Choose SIP Log File");
		lblChooseFile.setFont(new Font("Tahoma", Font.ITALIC, 18));
		lblChooseFile.setBounds(323, 16, 165, 20);
		panel.add(lblChooseFile);
		
		JButton button_1 = new JButton("DISCONNECT");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				try {
					connect.disconnect();
					Thread.currentThread().sleep(1000);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//is_connected=false;
                catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				lblNewLabel.setBackground(Color.RED);
				label_1.setBackground(Color.GREEN);  
				
			}
		});
		
		button_1.setBounds(30, 268, 136, 42);
		panel.add(button_1);
		
		statusbar = new JLabel("Output of your selection will go here");
		
		JButton btnChoose = new JButton("choose");
		btnChoose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				     		
				SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                    	
                    	chosenFile = chooser();
                    
                    }
                });  
				
			}
		});
		btnChoose.setBounds(349, 105, 115, 29);
		panel.add(btnChoose);
		
		
		
		JButton btnClear = new JButton("CLEAR");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textField.setText("");
				textField_1.setText("");
				
			}
		});
		btnClear.setBounds(176, 268, 115, 42);
		panel.add(btnClear);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(15, 310, 473, 0);
		panel.add(separator_2);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setOpaque(true);
		lblNewLabel.setForeground(Color.MAGENTA);
		lblNewLabel.setBackground(Color.RED);
		lblNewLabel.setBounds(0, 222, 15, 20);
		panel.add(lblNewLabel);
		
		label_1 = new JLabel("");
		label_1.setOpaque(true);
		label_1.setForeground(Color.MAGENTA);
		label_1.setBackground(Color.GREEN);
		label_1.setBounds(0, 279, 15, 20);
		panel.add(label_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(311, 52, 199, 26);
		panel.add(textField_2);
		
	}
	
	private String chooser()
	{
	    chooser = new JFileChooser();
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle("choosertitle");
	    //chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    chooser.setAcceptAllFileFilterUsed(false);

	    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) 
	    {
	      textField_2.setText(chooser.getSelectedFile().toString());
	      System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
	      System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
	      System.out.println(chooser.getSelectedFile().getName());
	      
	    } else {
	      System.out.println("No Selection ");
	    }
		return chooser.getSelectedFile().getName();
			
	}
}
