/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program �Excellence II / Aristeia II�, co-financed
*    by the European Union/European Social Fund - Operational
*    program �Education and Life-long Learning� and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.omg.CORBA.portable.OutputStream;


public class FileOperations {

	 // This function is used to receive a file
	 public static void receiveFile(Socket clientSocket) throws IOException {
		 	
			int bytesRead;
			DataInputStream clientData = new DataInputStream(clientSocket.getInputStream());

			String fileName = clientData.readUTF();
			FileOutputStream output = new FileOutputStream(("received_from_client_" + fileName));
			long size = clientData.readLong();
			byte[] buffer = new byte[1024];
			while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) 
			{
			   output.write(buffer, 0, bytesRead);
			   size -= bytesRead;
			}
		
	  }
      
	  // This function is used to send a file
	  public static void sendFile(String fileName, Socket sock) {
		
		  try {
		
				File myFile = new File(fileName);
				byte[] mybytearray = new byte[(int) myFile.length()];

				FileInputStream fis = new FileInputStream(myFile);
				BufferedInputStream bis = new BufferedInputStream(fis);
				DataInputStream dis = new DataInputStream(bis);
				dis.readFully(mybytearray, 0, mybytearray.length);

				OutputStream os = (OutputStream) sock.getOutputStream();

				//Send file name and size
				DataOutputStream dos = new DataOutputStream(os);//creating an output stream to send the file to the server
				dos.writeUTF(myFile.getName());
				dos.writeLong(mybytearray.length);
				dos.write(mybytearray, 0, mybytearray.length);//writing the contents of the file to the server
				dos.flush();
				System.out.println("File "+fileName+" sent to Server.");
				} catch (Exception e) {                           //reports error if any
				System.err.println("File does not exist!"); 
				}
		  
		}
}
