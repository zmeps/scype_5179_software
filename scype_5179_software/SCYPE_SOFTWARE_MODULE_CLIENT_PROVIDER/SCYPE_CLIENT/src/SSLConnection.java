/**
*    Author : Zisis Tsiatsikas
*    Email  : tzisis@aegean.gr
*
*    Description :
*
*    This module is part of the 5179 (SCYPE) research project,
*    implemented within the context of the Greek Ministry of
*    Development-General Secretariat of Research and Technology
*    funded program “Excellence II / Aristeia II”, co-financed
*    by the European Union/European Social Fund - Operational
*    program “Education and Life-long Learning” and National funds.
*
*    Date : 30/4/2015
*    Version : 1.0.0
*/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;


public class SSLConnection {

	private String IP;
	private int port;
	private SSLSocketFactory sslsocketfactory;
	private SSLSocket sslsocket;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private Socket socket;
	Integer bytesRead = 0;  
	int i=0;
	

	public SSLConnection(String IP, int port) throws IOException
	{
		this.IP = IP;
		this.port = port;
		// Appropriate properties for SSL
		System.setProperty("javax.net.ssl.trustStore", "/ClientKeyStore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "scype5179");
		
		// Creating socket and generating the handshake
		SSLSocketFactory f = (SSLSocketFactory) SSLSocketFactory.getDefault();
		sslsocket = (SSLSocket) f.createSocket(IP, port);
		sslsocket.startHandshake();
		 
		oos = new ObjectOutputStream(sslsocket.getOutputStream());
        ois = new ObjectInputStream(sslsocket.getInputStream()); 
        		
	}
	public void SSLconnect() throws UnknownHostException, IOException
	{
	    SSLSocketFactory f = (SSLSocketFactory) SSLSocketFactory.getDefault();
	    sslsocket = (SSLSocket) f.createSocket(IP, port);
	    sslsocket.startHandshake();
 
		oos = new ObjectOutputStream(sslsocket.getOutputStream());
        ois = new ObjectInputStream(sslsocket.getInputStream());		
	}
	public void connect() throws UnknownHostException, IOException
	{
	    // Create Sockets, create object streams for this socket.
		socket = new Socket(this.IP, this.port);
		oos = new ObjectOutputStream(socket.getOutputStream());
        ois = new ObjectInputStream(socket.getInputStream());
 
	}
	public void disconnect() throws IOException
	{
		// Send termination message to server, close streams and socket.
		oos.writeObject("quit");
        oos.flush();
	    oos.close();
	    ois.close();
	    sslsocket.close();
	}
	public void send_file(String fileName) throws IOException
	{
		
	    String send="SEND_FILE";
	    oos.writeObject(send);
	    oos.flush();
	        
        oos.writeObject(fileName); 
        oos.flush();
        
        final int BUFFER_SIZE = 100;  
        //oos.writeObject(file.getName());  
  
        FileInputStream fis = new FileInputStream("/"+fileName);  
        byte [] buffer = new byte[BUFFER_SIZE];  
               
        while ((bytesRead = fis.read(buffer)) > 0) {  
        
            oos.writeObject(bytesRead);  
            oos.writeObject(Arrays.copyOf(buffer, buffer.length)); 
            
        }  
	}
	
}
